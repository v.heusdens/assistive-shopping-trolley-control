"""Submodule of 'Toolbox' with general functions"""
import numpy as np
import os
import sympy as sp
import time
from collections import Iterable

from toolbox.transform import split_strings_in_list


#^ #######################################
def make_list(instance):
	"""Get 'instance' as a list if it isn't a list or a tuple.
	Args:		instance (-): instance to check
	Returns:	list or tuple: if 'instance' is a list or tuple, return unchanged. Otherwise return as list.
	."""
	if not isinstance(instance, Iterable) or isinstance(instance, str):
		return [instance]
	else:
		return instance


#^ #######################################
def setattr_multi(obj, **kwargs):
	"""Sets multiple attributes of obj at once.
    Use:
            setattr_multi(obj,
						  a = 1,
						  b = 2,
						  ...
            )
    ."""
	for k, v in kwargs.items():
		setattr(obj, k, v)


#^ #######################################
def getattr_multi(object, names):
	"""Gets multiple attributes of obj at once.
    object =    object to get attributes from
    names =     string of attribute names, separated by a space  
    ."""
	names = names.split()
	attributes = [getattr(object, name) for name in names]
	return attributes


#^ #######################################
def define_sympy_function_multi(names, dep_var):
	"""Makes multiple sympy functions depending on the same variable at once. 
    names =     iterable with function name strings  
    dep_var =   symbolic variable or string
    ."""
	if isinstance(dep_var, str):
		dep_var = sp.symbols(dep_var)

	names = split_strings_in_list(names)

	functions = [sp.Function(name)(dep_var) for name in names]
	return functions


#^ #######################################
def define_sympy_derivative_multi(functions, dep_var, amount=1):
	"""Makes multiple sympy derivatives depending on the same variables at once. 
    functions =     iterable with functions to take derivative from 
    dep_var =   symbolic variable or string
    ."""
	if isinstance(dep_var, str):
		dep_var = sp.symbols(dep_var)

	derivatives = [sp.Derivative(function, dep_var, amount) for function in functions]
	return derivatives


## MAIN #######################################
if __name__ == "__main__":
	pass
