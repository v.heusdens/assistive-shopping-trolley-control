"""Submodule of 'Toolbox' with functions related to the transformation of equations"""

import re
import sympy as sp


#^ #######################################
def convert_string_to_sympy(string, vars_obj, var_indicator='s'):
	""" Transforms string to sympy expression using parameters given in class 'vars'.
	Args:
		string (str):  string which needs to be transformed.
		vars_obj (class): class with used variables defined as properties
		var_indicator (str, optional): name of vars class in string. Defaults to 's'.
	"""
	from sympy.core.compatibility import exec_

	ns = {var_indicator: vars_obj}
	exec_('import sympy as sp', ns)

	return sp.sympify(string, locals=ns)


#^ #######################################
def convert_latex_to_sympy(eq, vars, var_pref='s'):
	""" Transforms copied latex equation to python equation.
	Args:
		eq (eq or str): [equation to be transformed]
		vars (object, optional): Object containing variables to be filled in in eq. Defaults to ParamsSymbolic().
	"""
	eq = str(eq)
	eq = add_sympy_prefix(eq)

	#% clean up string
	#^ Replace stuff
	rplc = [('\x08', 'b'), ('[', '('), (']', ')'), ('^', '**')]
	for old, new in rplc:
		eq = eq.replace(old, new)

	#^ remove special characters
	for rmv in ['\\nonumber', '%.*?(?:\\t)', '\\t', r'\\', '&', '[Bb]ig+[^\W_]?']:
		eq = re.sub(rmv, '', eq)

	#^ remove spacing-related stuff
	for rmv in ['qquad', 'quad']:
		eq = eq.replace(rmv, '')

	eq = convert_sympy_to_sympy_printed(eq)
	return convert_string_to_sympy(eq, vars, var_indicator='s')


#^ #######################################
def convert_sympy_to_latex(line):
	"""Do all latex conversions."""
	from collections import OrderedDict

	def _convert_equation(line):
		"""Convert "sp.Eq(left, right)" to "left & = right"""
		match = re.search(r"sp.Eq\(([^,]+),(.*)\)", line)
		if match:
			line = "{} & = {}".format(match.group(1), match.group(2))
		return line

	def _convert_with_mapping(line, mapping):
		"""Replace any occurence of "old" with "new" in line
		:param mapping: Dictionary {"old": "new"}
		."""
		for old, new in mapping.items():
			line = line.replace(old, new)
		return line

	def _convert_subscript(line):
		"""Convert "xxx_yy" to "xxx_{YY}"""

		def replacer(match):
			subscript_part = match.group(2)
			if "\\" not in subscript_part:
				subscript_part = subscript_part.upper()
			return match.group(1) + "_{" + subscript_part + "}"

		return re.sub(r"(\w+)_(\\?\w+)", replacer, line)

	def _replace_underscore_with_quote(line):
		"""Convert "xxx_" to "xxx'" """
		return re.sub(r"([\w_]+)_([^\w])", r"\1'\2", line)

	def _remove_underscore_in_derivatives(line):
		"""Convert "\(d)dot_{}" to '\(d)dot{}' """
		return re.sub(r"(\\dd?ot)_({[\w\\]*})", r"\1\2", line)

	line = str(line)
	# Note that the order of these functions is important!
	mapping = OrderedDict(
	    [
	        ("sp.sin", "sin"), ("sp.cos", "cos"), ("sp.tan", "tan"), ("sin", r"\sin"), ("cos", r"\cos"), ("tan", r"\tan"),
	        ("gamma", r"\gamma"), ("theta", r"\theta"), ("beta", r"\beta"), ("1.0*", ""), ("1.0 *", "")
	    ] + [("{}.0*".format(i), "{}*".format(i)) for i in range(2, 10)]  # "3.0*" -> "3*"
	    + [
	        ("s.", ""),
	        ("dd_", "\ddot_"),
	        ("d_", "\dot_"),
	        ("mu", "\mu"),
	        ("**", "^"),
	        ("*", " "),
	    ]
	)
	line = _convert_equation(line)
	line = _replace_underscore_with_quote(line)
	line = _convert_with_mapping(line, mapping)
	line = _convert_subscript(line)
	line = _remove_underscore_in_derivatives(line)
	return line


#^ #######################################
def convert_sympy_to_sympy_printed(eq, var_pref='s'):
	"""Transforms sympy equation to string which can be copied directly to python"""

	eq = str(eq)

	#^ fix subscripts
	regex = '(_{*\w*}*)(.?)'  #@ find all word characters directly after a '_', optionally surrounded by '{}'. Also get trailing character.
	for fix, trail in re.findall(regex, eq):
		rplc = re.sub('[{}]', '', fix).lower()
		if trail.isalpha() or trail.isdigit():
			rplc = rplc + ' '

		eq = eq.replace(fix + trail, rplc + trail)

	#^ fix derivatives
	regex = '([d]+)(ot{)(.+?)(})(.)'  #@ find all instances of dot{...} with an arbitrary amount of d's. Also get trailing character.
	for d, v1, var, v2, trail in re.findall(regex, eq):
		rplc = d + '_' + var
		if trail.isalpha() or trail.isdigit():
			rplc = rplc + ' '
		eq = eq.replace(d + v1 + var + v2 + trail, rplc)

	#% add multipliers
	reg1 = '([\d\w]+\s*)(\()'  #@ find all sets where '(' is followed by a character or digit. Ignore spaces.
	reg2 = '(\))(\s*[\d\w(])'  #@ find all sets where ')' is followed by a character, digit or '('. Ignore spaces.
	reg3 = '([\w\d])(\s+[\w\d])'  #@ find all sets where characters or digits are only separated by an arbitrary amount of spaces

	for regex in [reg1, reg2, reg3]:
		for left, right in re.findall(regex, eq):
			if any(item in left for item in ['sin', 'cos', 'tan', 'sign']):
				continue
			eq = eq.replace(left + right, left + '*' + right)

	#% add sympy prefix
	for x in ['sin', 'cos', 'tan', 'sign']:
		eq = eq.replace(x, 'sp.' + x)
		eq = eq.replace('sp.sp.', 'sp.')

	#% add prefix before params
	for slicer in ['+', '-', '*', '/', '(']:
		tmp = eq.split(slicer)
		tmp = [x.strip() for x in tmp]
		tmp = [
		    var_pref + '.' + x if
		    (len(x) > 0 and x[0].isalpha() and not x[:3] == 'sp.' and not x[:2] == var_pref + '.') else x for x in tmp
		]
		eq = slicer.join(tmp)

	#% fix remaining curly braces
	eq = eq.replace('{', '(')
	eq = eq.replace('}', ')')
	return eq


#^ #######################################
def add_sympy_prefix(model):
	"""Adds 'sp' before sympy functions."""
	if not isinstance(model, str):
		model = str(model)
	for inst in ["sin", "cos", "tan", "sign"]:
		model = model.replace(inst, "sp." + inst)
	for inst in ["(t)"]:
		model = model.replace(inst, '')
	return model


#^ #######################################
def split_strings_in_list(names, split=' '):
	"""Split strings in a list separated by 'split' character into separate list entries"""
	return [elem for string in names for elem in string.split(split)]


#^ #######################################
def expand_to_variables(model, vars):
	"""Expands equation into coefficients of variables given in 'var' list.
		Args:
			model (symbolic equation): equation to expand  
			vars (iterable): iterable of variables to expand to
		Returns:
			dict: equation parts per variable
	"""
	for var in vars:
		expr = sp.collect(expr, var)
		expr_parts[str(var)] = expr.coeff(var, 1)
		expr -= expr_parts[str(var)] * var
		expr_parts[str(var)] = sp.simplify(expr_parts[str(var)])
	expr_parts["rest"] = expr

	return expr_parts


#^ #######################################
def subs_multi(f, x, y, replacements=None):
	""" subs using iterables, recursive; In f, replace x by y.
	f : objective function  
	x : variables to be replaced  
	y : variables to use as replacement  
	"""
	if replacements is None:
		replacements = zip(x, y)

	if isinstance(f, list):  #@ Go deeper
		f = list(f)
		for i, _ in enumerate(f):
			f[i] = subs_multi(f[i], x, y, replacements)

	else:  #@ End of tree
		while f.has(*x):
			f = f.subs(replacements)
		return f

	return f
