"""Submodule of 'Toolbox' with package-related functions"""

import numpy as np

from toolbox.ros import load_config
from toolbox.transform import convert_string_to_sympy


#^ #######################################
def load_q_filter(config='trolley_controller'):
	"""Get controller q_filter numerator and denominator from 'config_controller' as lists.
	Args:		config (str, optional): package name from where to load config file containing filter numerator and denominator. Defaults to 'trolley_controller'.
	Returns:	list, list: numerator and denominator list of q_filter
	"""
	config_controller = load_config(config)  #@ load configuration file

	#@ transform numerator and denominator to python-code
	num = [float(convert_string_to_sympy(x, np.pi, 'pi')) for x in config_controller['numerator']]
	denom = [float(convert_string_to_sympy(x, np.pi, 'pi')) for x in config_controller['denominator']]

	return num, denom
