"""Submodule of 'Toolbox' with computation-related functions"""

import numpy as np
import sympy as sp

from toolbox.general import make_list


#^ #######################################
def avg(min, max):
	"""Calculate average from numbers"""
	return (min + max) / 2.


#^ #######################################
def mean(lst):
	"""Calculate mean from list"""
	return avg(min(lst), max(lst))


#^ #######################################
def pnorm(a, p=2):
	"""Returns p-norm of sequence a. By default, p=2. 
    p must be a positive integer.
    ."""
	a = make_list(a)

	if p == 'inf' or np.isinf(p):  # Infinity norm - largest absolute value
		return max(abs(x) for x in a)
	else:
		return np.sum(x**p for x in a)**(sp.Rational(1, p))


#^ #######################################
def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
	"""Compare floats, taking into account roundoff-errors
	Args:
		a, b (float): numbers to compare
		rel_tol (float, optional): tolerance which scales with magnitude of arguments. Defaults to 1e-09.
		abs_tol (float, optional): absolute tolerance, always applied. Defaults to 0.0.
	Returns:
		bool: arguments considered equal or not
	."""
	return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


#^ #######################################
def jacobian(f, x):
	"""
	Derivatives
    f = objective function  
    x = variables to differentiate to  
    ."""
	j = [sp.diff(f, i) for i in x]  #@ symbolic jacobian
	return j


#^ #######################################
def hessian(f, x):
	"""Double derivatives"""

	j = jacobian(f, x)  #@ symbolic jacobian
	h = [[sp.diff(j_, i) for i in x] for j_ in j]  #@ symbolic hessian
	return h


#^ #######################################
def derivative_multi(f, x, order=1):
	"""
	takes the derivative of multiple equations simultaneously
    f = iterable of equations  
    x = variable or iterable of variables to differentiate to  
    i = order of derivative
    ."""
	x = make_list(x)

	df = [[sp.diff(F, X, order) for X in x] for F in f]
	df = np.array(df)
	return df


## MAIN #######################################
if __name__ == "__main__":
	pass
