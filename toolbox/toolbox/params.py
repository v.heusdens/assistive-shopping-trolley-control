"""Submodule of 'Toolbox' with parameter-related functions"""

import sympy as sp
from numbers import Number

from toolbox.compute import avg
from toolbox.transform import subs_multi
from toolbox.general import define_sympy_derivative_multi, define_sympy_function_multi
from toolbox.ros import load_config
from toolbox.general import make_list


#^ #######################################
class ParamsGroups:
	""" Contains groups referencing parameters"""

	def __init__(s):
		s.params_uncertain = ('mu', 'm_b', 'L_h', 'gamma')
		s.param_certain = ('L_wh', 'L_b', 'W_f', 'W_b', 'R_w', 'm_f', 'm_w', 'g')
		s.params_dependent = ('m_ft', 'F_fg', 'F_wg', 'L_wf', 'W_wf', 'beta', 'I_w')

		s.params_inputs = ('F_hx', 'F_hy', 'T_m')
		s.params_forces = s.params_inputs + ('F_fg', 'F_wg', 'T_roll')
		s.params_position = ('xw', 'yw', 'theta', 'xf', 'yf', 'beta')
		s.params_velocity = ('d_xw', 'd_yw', 'd_theta', 'd_xf', 'd_yf', 'd_beta')
		s.params_acceleration = ('dd_xw', 'dd_yw', 'dd_theta', 'dd_xf', 'dd_yf', 'dd_beta')
		s.params_implicit = ('F_wx', 'F_wy', 'F_stat', 'F_n')


#^ #######################################
class ParamsUncertain:
	"""	Contains information about uncertain parameters + functions to obtain avg, lim, etc."""

	def __init__(self):
		_params = load_config('trolley_description')

		#% Friction coefficient mu
		self.mu_min = _params['friction_coeff_min']
		self.mu_max = _params['friction_coeff_max']
		self.mu_nom = _params['friction_coeff_nom']
		self.mu_avg = self.get_avg('mu')

		#% Bag mass m_b
		self.m_b_bag = _params['bag_mass']
		self.m_b_min = self.m_b_bag
		self.m_b_max = self.m_b_bag + _params['bag_mass_added']
		self.m_b_nom = self.m_b_bag + _params['bag_mass_added_nom']
		self.m_b_avg = self.get_avg('m_b')

		#% Handle height L_h
		self.L_h_min = _params['handle_height_min']
		self.L_h_max = _params['handle_height_max']
		self.L_h_nom = _params['handle_height_nom']
		self.L_h_avg = self.get_avg('L_h')

		#% Slope gamma
		self.gamma_min = 0
		self.gamma_max = sp.atan(1. / 10)
		self.gamma_nom = 0
		self.gamma_avg = self.get_avg('gamma')

	def get_min(self, name):
		"""return minimal value of specified uncertain parameter"""
		return getattr(self, str(name) + '_min')

	def get_max(self, name):
		"""return maximal value of specified uncertain parameter"""
		return getattr(self, str(name) + '_max')

	def get_nom(self, name):
		"""return nominal value of specified uncertain parameter"""
		return getattr(self, str(name) + '_nom')

	def get_lim(self, name):
		"""return list of limit values of specified uncertain parameter"""
		return (self.get_min(name), self.get_max(name))

	def get_avg(self, name):
		"""return nominal value of specified uncertain parameter"""
		return avg(*self.get_lim(name))

	def get_params_lim(s):
		"""return list of list of limits of uncertain parameter values"""
		return [self.get_lim(name) for name in ParamsGroups().params_uncertain]

	def get_params_avg(s):
		"""return list of average uncertain parameter values"""
		return [self.get_avg(name) for name in ParamsGroups().params_uncertain]

	def get_params_min(s):
		"""return list of minimal uncertain parameter values"""
		return [self.get_min(name) for name in ParamsGroups().params_uncertain]

	def get_params_max(s):
		"""return list of maximal uncertain parameter values"""
		return [self.get_max(name) for name in ParamsGroups().params_uncertain]

	def get_params_nom(s):
		"""return list of nominal uncertain parameter values"""
		return [self.get_nom(name) for name in ParamsGroups().params_uncertain]


#^ #######################################
class ParamsNumeric(object):
	""" Contains parameter values, yet undefined set to 'None', + function to obtain all defined parameters """

	def __init__(self):
		_params = load_config('trolley_description')

		#% Independent params
		self._L_wh = _params['frame_height']
		self._W_f = _params['frame_depth']
		self._L_b = _params['bag_height']
		self._W_b = _params['bag_depth']
		self._R_w = _params['wheel_radius']
		self._m_f = _params['frame_mass']
		self._m_w = _params['wheel_mass']
		self._g = _params['gravity']

		self._m_b = None
		self._L_h = None

		self._mu = None
		self._gamma = None

		self._F_hx = None
		self._F_hy = None
		self._T_m = None

		self._d_beta = None
		self._d_xw = None
		self._dd_xw = None

		self.T_roll = None

	#% Dependent params
	@property
	def m_ft(s):
		return s.m_f + s.m_b

	@property
	def m_acc(s):
		return s.m_ft + 3 * s.m_w

	@property
	def F_fg(s):
		return s.m_ft * s.g

	@property
	def F_wg(s):
		return s.m_w * s.g

	@property
	def L_wf(s):
		return 1. / s.m_ft * (s.m_f / 2. * s.L_wh + s.m_b / 3. * s.L_b)

	@property
	def W_wf(s):
		return s.m_b / s.m_ft * (s.W_f / 2. + s.W_b / 3.)

	@property
	def L_h_acc(s):
		return s.L_h - s.R_w

	@property
	def beta(s):
		return sp.asin((s.L_h - s.R_w) / s.L_wh)

	@property
	def I_w(s):
		return 0.5 * s.m_w * s.R_w**2

	@property
	def I_f(s):
		return 1 / 3. * s.m_ft * s.L_wh**2  #@ Rod rotating around one end

	@property
	def I(s):
		return s.R_w**2 * (s.m_ft + 3. * s.m_w)

	#^ GETTERS/SETTERS ####################################
	def __getattribute__(self, name):
		"""Replace standard __getattribute__ method. If property is explicitly set (has '_' prefix), return that one. Otherwise return standard property value. If value is 'None', raise exception."""
		try:
			obj = object.__getattribute__(self, '_' + name)
		except AttributeError:
			try:
				obj = object.__getattribute__(self, name)
			except:
				raise

		if obj is None:
			string = 'Value of {} not yet set!'.format(name)
			raise ValueError(string)
		else:
			return obj

	def __setattr__(self, name, value):
		"""Replace standard __setattr__ method. Add '_' prefix to name to distinguish between standard and explicitly set property values"""
		if name[0] != '_':
			name = '_' + name
		object.__setattr__(self, name, value)

	#^ PROPERTIES ##################################
	@property
	def params_defined(self):
		"""Returns dict with defined params and their values"""
		params = {name.replace('_', '', 1): value for name, value in self.__dict__.items() if isinstance(value, Number)}
		return params

	@property
	def params_undefined(self):
		"""Returns list with undefined params"""
		return [name.replace('_', '', 1) for name, value in self.__dict__.items() if value is None]

	@property
	def params_defined_values(self):
		defined_params = self.params_defined
		return defined_params.values()

	#^ REGULAR FUNCTIONS #######################################
	def get_params_defined(self):
		"""Returns dict with defined params and their values"""
		return self.params_defined

	def get_params_undefined(self):
		"""Returns list with undefined params"""
		return self.params_undefined

	def set_params_symbolic(self, params):
		"""set (list of) params (str or symbolic) to symbolic variable"""
		sym = ParamsSymbolic()
		params = make_list(params)

		for param in params:
			param = str(param)
			setattr(self, param, getattr(sym, param))

	def set_params_to_type(self, params, par_type):
		"""params = (list of) params to be set (str or symbolic), par_type = [min|max|avg|nom]"""
		params = make_list(params)

		for param in params:
			param = str(param)
			setattr(self, param, getattr(ParamsUncertain(), param + '_' + par_type))
		print('params {} set to their {} value'.format(params, par_type))

	def set_params_unspecified_symbolic(self):
		"""set all unset params to symbolic variables"""
		params = self.get_params_undefined()
		self.set_params_symbolic(params)

	def set_params_min(self):
		"""set all uncertain params to their minimal values"""
		params = ParamsGroups().params_uncertain
		self.set_params_to_type(params, 'min')

	def set_params_max(self):
		"""set all uncertain params to their maximal values"""
		params = ParamsGroups().params_uncertain
		self.set_params_to_type(params, 'max')

	def set_params_avg(self):
		"""set all uncertain params to their average values"""
		params = ParamsGroups().params_uncertain
		self.set_params_to_type(params, 'avg')

	def set_params_nom(self):
		"""set all uncertain params to their nominal values"""
		params = ParamsGroups().params_uncertain
		self.set_params_to_type(params, 'nom')


#^ #######################################
class ParamsSymbolic:
	""" Symbolic variable definitions and dependencies """

	def __init__(s, expand=False, T_roll_nonlin=False):
		if expand is False:
			expand = ['T_roll']  #@ Default

		#% Dimensions
		s.L_wh, s.W_f, s.L_b, s.W_b, s.L_h, s.R_w = sp.symbols("L_wh W_f L_b W_b L_h R_w", real=True, nonnegative=True)
		s.L_bc, s.W_bc, s.L_fc, s.L_wf, s.W_wf = sp.symbols('L_bc W_bc L_fc L_wf W_wf', real=True, nonnegative=True)

		#% Angles
		s.gamma = sp.symbols('gamma', real=True)

		#% Masses and inertial
		s.m_f, s.m_b, s.m_w, s.m_ft, s.I_f, s.I_w = sp.symbols('m_f m_b m_w m_ft I_f I_w', real=True, nonnegative=True)

		#% Forces
		s.F_hx, s.F_hy, s.T_m, s.F_fg, s.F_wg, s.F_wx, s.F_wy, s.F_n, s.F_stat, s.T_roll = sp.symbols(
		    'F_hx F_hy T_m F_fg F_wg F_wx F_wy F_n F_stat T_roll', real=True
		)

		#% Other
		s.mu, s.g, s.C, s.d = sp.symbols('mu g C d', real=True, nonnegative=True)

		#% Variables
		s.xw, s.yw, s.theta, s.xf, s.yf, s.beta = sp.symbols('xw yw theta xf yf beta', real=True)
		s.d_xw, s.d_yw, s.d_theta, s.d_xf, s.d_yf, s.d_beta = sp.symbols('d_xw d_yw d_theta d_xf d_yf d_beta', real=True)
		s.dd_xw, s.dd_yw, s.dd_theta, s.dd_xf, s.dd_yf, s.dd_beta = sp.symbols(
		    'dd_xw dd_yw dd_theta dd_xf dd_yf dd_beta', real=True
		)

		#% Dependent on other variables (dep. on indep. variables)
		s.L_h_acc = s.L_h - s.R_w
		s.m_ft_dep = s.m_f + s.m_b
		s.F_wg_dep = s.m_w * s.g
		s.I_w_dep = 0.5 * s.m_w * s.R_w**2
		s.beta_dep = sp.asin(s.L_h_acc / s.L_wh)
		s.T_roll_dep = s.mu * s.F_n * s.R_w
		s.T_roll_nonlin_dep = s.T_roll_dep * sp.sign(s.d_xw)

		#% Of t
		s.t = sp.symbols('t')
		s.xw_t, s.yw_t, s.theta_t, s.xf_t, s.yf_t, s.beta_t = define_sympy_function_multi(['xw yw theta xf yf beta'], s.t)
		s.d_xw_t, s.d_yw_t, s.d_theta_t, s.d_xf_t, s.d_yf_t, s.d_beta_t = define_sympy_derivative_multi(
		    [s.xw_t, s.yw_t, s.theta_t, s.xf_t, s.yf_t, s.beta_t], s.t
		)
		s.dd_xw_t, s.dd_yw_t, s.dd_theta_t, s.dd_xf_t, s.dd_yf_t, s.dd_beta_t = define_sympy_derivative_multi(
		    [s.xw_t, s.yw_t, s.theta_t, s.xf_t, s.yf_t, s.beta_t], s.t, 2
		)

		s.expand(expand, T_roll_nonlin)

	## Parameter dependencies (dep. on dep. variables)
	@property
	def m_acc(s):
		return s.m_ft + 3 * s.m_w

	@property
	def F_fg_dep(s):
		return (s.m_ft * s.g)

	@property
	def L_wf_dep(s):
		return (1. / s.m_ft * (s.m_f / 2. * s.L_wh + s.m_b / 3. * s.L_b))

	@property
	def W_wf_dep(s):
		return (s.m_b / s.m_ft * (s.W_f / 2. + s.W_b / 3.))

	@property
	def L_dep(s):
		(s.L_wf * (sp.sin(s.beta) - s.mu * sp.cos(s.beta)) + s.W_wf * (s.mu * sp.sin(s.beta) + sp.cos(s.beta)))

	#^ Functions #######################################
	def expand(self, params=None, T_roll_nonlin=False):
		"""Expand dependent parameters"""

		if params is None:  #@ Do nothing
			return
		elif params is True:  #@ Expand all dependent parameters
			groups = ParamsGroups()
			params = groups.params_dependent

		replacements = [getattr(self, param + '_dep') for param in params]

		params = [getattr(self, key) for key in params]

		if T_roll_nonlin and self.T_roll in params:
			i = params.index(self.T_roll)
			replacements[i] = self.T_roll_nonlin_dep

		while True:
			for key, value in self.__dict__.items():
				setattr(self, key, subs_multi(value, params, replacements))

			free_symbols = [str(entry) for par in self.__dict__ for entry in getattr(self, par).free_symbols]
			free_symbols = set(free_symbols)

			if not any(par in free_symbols for par in params):
				return

	def get_symbolic_params(self, params):
		"""Returns list of symbolic params from string"""

		return [getattr(self, param) for param in params]


#^ #######################################
class TransferFunctions:

	def __init__(s):
		sym = ParamsSymbolic(expand=True)

		#% Symbolic
		s.A, s.An = sp.symbols("A An", nonnegative=True)
		s.B, s.Bn = sp.symbols("B Bn", real=True)
		s.G, s.Gn, s.Ga = sp.symbols("G Gn Ga", real=True)
		s.P, s.Pn, s.Pa = sp.symbols("P Pn Pa", real=True)
		s.Q = sp.symbols("Q")

		#% Functions
		s.TF_A = (
		    sym.mu * sym.m_ft * (sym.L_wf * sp.sin(sym.beta) + sym.W_wf * sp.cos(sym.beta)) + sym.L_wf * sp.cos(sym.beta) *
		    (sym.m_ft + 3 * sym.m_w)
		)**(-1)
		s.TF_B = 2 * (sym.L_wh * sp.cos(sym.beta) * sym.R_w**(-1) - sym.mu)
		s.TF_G = s.TF_A * s.TF_B

	def get_tf_num(self, name):
		"""Return transfer function filled with known param values"""

		num = ParamsNumeric()
		tf = getattr(self, 'TF_' + name)
		symbols = tf.free_symbols

		for symbol in symbols:
			try:
				value = getattr(num, str(symbol))
				tf = tf.subs(symbol, value)
			except:
				pass

		return tf

	def get_tf_type(self, name, type):
		"""Return filled transfer function with params of 'type' (min, max, avg, nom)"""

		unc = ParamsUncertain()
		eq = getattr(unc, "get_" + type)
		tf = self.get_tf_num(name)
		symbols = tf.free_symbols

		for symbol in symbols:
			try:
				value = eq(symbol)
				tf = tf.subs(symbol, value)
			except:
				print("Warning: symbol '{}' not found in uncertain params".format(symbol))

		return tf

	def get_tf_avg(self, name):
		"""Return transfer function with average values"""
		return self.get_tf_type(name, 'avg')

	def get_tf_nom(self, name):
		"""Return transfer function with nominal values"""
		return self.get_tf_type(name, 'nom')

	def get_tf_min(self, name):
		"""Return transfer function with minimal values"""
		return self.get_tf_type(name, 'min')

	def get_tf_max(self, name):
		"""Return transfer function with maximal values"""
		return self.get_tf_type(name, 'max')


#^ #######################################
if __name__ == "__main__":
	g = ParamsGroups()
	u = ParamsUncertain()
	n = ParamsNumeric()
	s = ParamsSymbolic()
	tf = TransferFunctions()
