"""Submodule of 'Toolbox' with ROS-related functions"""

import rospkg
import rospy
import time
import yaml


#^ #######################################
def load_config(pkg_name='general'):
	"""Loads config file from specified package within current workspace, given the following package structure:
    package -> config -> config_'pkg_name'.yaml
    ."""

	folder = '/config'
	name = '/config_' + pkg_name.replace('trolley_', '')
	type = '.yaml'

	if 'general' in pkg_name:
		path = rospkg.RosPack().get_path('toolbox') + '/..'
	else:
		path = rospkg.RosPack().get_path(pkg_name)

	config = yaml.safe_load(open(path + folder + name + type))
	return config


#^ #######################################
def wait_on_simulation_start(loop_delay, timeout=False, NODE_NAME=""):
	"""Waits on Gazebo-time to start. Includes an optional timeout.
	Set timeout to 'False' to disable."""

	string = "Timeout disabled"
	if timeout is not False:
		_timeout = time.time() + timeout
		string = "Timeout set to {} s".format(timeout)

	try:
		while rospy.get_time() == 0:
			rospy.loginfo_once('HOLD: Waiting on simulation start... ' + string, logger_name=NODE_NAME)
			time.sleep(loop_delay)

			if timeout is not False and time.time() > _timeout:
				string = "HOLD: Timeout: rostime not running for {} s".format(timeout)
				raise Exception(string)
				return

	except rospy.ROSInterruptException:
		rospy.logwarn("HOLD: Node shutdown before timeout", logger_name=NODE_NAME)

	rospy.loginfo('HOLD: Simulation started, continuing', logger_name=NODE_NAME)


#^ #######################################
def hold_ros(duration, loop_delay, NODE_NAME="", loginfo=True):
	"""Holds process for a certain time in rostime before continuing."""
	if loginfo:
		rospy.loginfo('HOLD: Waiting {} s...'.format(duration), logger_name=NODE_NAME)

	duration = rospy.get_time() + duration
	while not rospy.is_shutdown() and rospy.get_time() < duration:
		rospy.sleep(loop_delay)

	if loginfo:
		rospy.loginfo('HOLD: Finished waiting, continuing'.format(duration), logger_name=NODE_NAME)
