#!/usr/bin/env python

"""Unit test for converter_node"""

import rospy
import time
import unittest
from numpy import sign

from std_msgs.msg import Float64

from toolbox.ros import load_config

PKG = 'trolley_tests'
NODE_NAME = 'test_converter_node'
FILTERED_DISTURBANCE_TOPIC = '/trolley/controller/disturbance_estimate_filtered'
MOTOR_COMMAND_TOPIC = '/trolley/actuators/motor_torque'


#^ CLASS #######################################
class TestConverterNode(unittest.TestCase):

	def setUp(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_general = load_config('trolley_general')
		config_description = load_config('trolley_description')
		config_tests = load_config('trolley_tests')

		self.loop_delay = config_general['loop_delay']
		self.timeout = config_tests['timeout']
		self.data_received = None
		self.R_w = config_description['wheel_radius']

		rospy.loginfo('Timeout set to {} s'.format(self.timeout), logger_name=NODE_NAME)

		#^ init publishers/subscribers/services
		rospy.Subscriber(MOTOR_COMMAND_TOPIC, Float64, self.callback)
		rospy.loginfo('Subscribed to {}'.format(MOTOR_COMMAND_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def callback(self, msg):
		rospy.loginfo_once('Message received on topic {}'.format(MOTOR_COMMAND_TOPIC), logger_name=NODE_NAME)
		self.data_received = msg.data

	#^ #######################################
	def test_if_publishing(self):
		timeout = rospy.get_time() + self.timeout

		while not rospy.is_shutdown() and self.data_received is None:
			if rospy.get_time() >= timeout:
				string = "Timeout: no messages received on {} for a duration of {} s".format(
					MOTOR_COMMAND_TOPIC, self.timeout)
				rospy.logerr(string, logger_name=NODE_NAME)
				return

			rospy.sleep(self.loop_delay)

		string = "No messages received on topic {}".format(MOTOR_COMMAND_TOPIC)
		self.assertFalse(self.data_received is None, string)

	#^ #######################################
	def test_command_sign(self):
		msg = rospy.wait_for_message(FILTERED_DISTURBANCE_TOPIC, Float64, timeout=self.timeout)
		sign_dist_torque = sign(msg.data)
		sign_motor_torque = sign(self.data_received)

		string = "RESULT: Sign of estimated disturbance={}; sign of motor torque signal={}".format(
			sign_dist_torque, sign_motor_torque)
		rospy.loginfo(string, logger_name=NODE_NAME)
		self.assertTrue(sign_motor_torque, sign_dist_torque)

	#^ #######################################
	def test_command_magnitude(self):
		msg = rospy.wait_for_message(FILTERED_DISTURBANCE_TOPIC, Float64, timeout=self.timeout)
		dist_force = msg.data
		dist_torque_mag = abs(dist_force * self.R_w)
		motor_torque_mag_per_wheel = abs(self.data_received)

		string = "RESULT: Disturbance torque magnitude={}; motor command magnitude per wheel={}".format(
			dist_torque_mag, motor_torque_mag_per_wheel)
		rospy.loginfo(string, logger_name=NODE_NAME)
		self.assertAlmostEqual(motor_torque_mag_per_wheel, 0.5 * dist_torque_mag)


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest
	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
	rostest.rosrun(PKG, NODE_NAME, TestConverterNode)
