#!/usr/bin/env python

"""Unit test for distance_tracker_node"""

import numpy as np
import rospy
import unittest

from std_msgs.msg import Float64

from toolbox.compute import isclose
from toolbox.ros import hold_ros, load_config

PKG = 'trolley_tests'
NODE_NAME = 'test_distance_tracker_node'
SUBSCRIBER_TOPIC = '/trolley/analysis/traveled_distance'


#^ CLASS #######################################
class TestDistanceTrackerNode(unittest.TestCase):

	def setUp(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_general = load_config('trolley_general')
		config_sensors = load_config('trolley_sensors')
		config_tests = load_config('trolley_tests')

		self.loop_delay = config_general['loop_delay']
		self.gather_duration = config_tests['gather_duration']
		self.timeout = config_tests['timeout']

		rospy.loginfo('Timeout set to {} s'.format(self.timeout), logger_name=NODE_NAME)

		link_states_data = config_tests['gazebo_link_states_publisher_data']
		self.delta_distance = self.calculate_delta_distance_per_time_step(
			link_states_data)  #@ Change in distance per time-step

		self.msg_counter = 0
		self.distance_topic = 0
		self.min_msg_amount = config_sensors[
			'update_rate']  #@ Minimum amount of messages that should be received (1 s)

		self.trigger_gather_data = False  #@ True if data gathering should take place right now

		#^ init publishers/subscribers/services
		rospy.Subscriber(SUBSCRIBER_TOPIC, Float64, self.callback)
		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def callback(self, msg):
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

		if not self.trigger_gather_data:
			rospy.loginfo_throttle(0.5,
				"Gather data trigger set to false. Skipping data point",
				logger_name=NODE_NAME)
			return

		self.msg_counter += 1
		self.distance_topic += self.delta_distance

	#^ #######################################
	def calculate_delta_distance_per_time_step(self, data):
		delta_distance_x = data['pose']['position']['x']
		delta_distance_z = data['pose']['position']['z']
		delta_distance = np.sqrt(delta_distance_x**2 + delta_distance_z**2)
		return delta_distance

	#^ #######################################
	def hold_during_data_gathering(self):
		hold_ros(self.gather_duration, self.loop_delay, NODE_NAME)

	#^ #######################################
	def test_distance_tracker_node(self):
		#^ wait for messages to be published
		rospy.wait_for_message(SUBSCRIBER_TOPIC, Float64, timeout=self.timeout)

		#^ gather data
		rospy.loginfo_once('Gathering data...', logger_name=NODE_NAME)
		self.trigger_gather_data = True
		self.hold_during_data_gathering()
		self.trigger_gather_data = False

		#^ check if enough data points received
		if self.msg_counter < self.min_msg_amount:
			string = "Did not receive enough data. Amount: {}".format(self.msg_counter)
			rospy.logerr(string, logger_name=NODE_NAME)
			self.fail(string)

		#^ test data
		distance_expected = self.delta_distance * self.msg_counter

		string = "Distance per time step: {}; # time steps: {}; total distance calculated: {}; total distance from topic: {}".format(
			self.delta_distance, self.msg_counter, distance_expected, self.distance_topic)
		rospy.loginfo('TEST RESULT: {}'.format(string), logger_name=NODE_NAME)
		self.assertTrue(isclose(distance_expected, self.distance_topic))


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest
	#unittest.main()
	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
	rostest.rosrun(PKG, NODE_NAME, TestDistanceTrackerNode)
