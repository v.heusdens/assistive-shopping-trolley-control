#!/usr/bin/env python

"""Unit test for velocity_reader_node"""

import rospy
import unittest

from gazebo_msgs.msg import LinkStates
from std_msgs.msg import Float64

from toolbox.ros import hold_ros, load_config

PKG = 'trolley_tests'
NODE_NAME = 'test_velocity_reader_node'
VELOCITY_READER_TOPIC = '/trolley/sensors/velocity'
GAZEBO_LINK_STATES_TOPIC = '/gazebo/link_states'


#^ CLASS #######################################
class VelocityReaderNodeTest(unittest.TestCase):

	def setUp(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_general = load_config('trolley_general')
		config_tests = load_config('trolley_tests')

		self.loop_delay = config_general['loop_delay']
		self.timeout = config_tests['timeout']

		self.data_received = None

		rospy.loginfo('Timeout set to {} s'.format(self.timeout), logger_name=NODE_NAME)

		#^ init publishers/subscribers/services
		rospy.Subscriber(VELOCITY_READER_TOPIC, Float64, self.callback)
		rospy.loginfo('Subscribed to {}'.format(VELOCITY_READER_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def callback(self, msg):
		rospy.loginfo_once('Message received on topic {}'.format(VELOCITY_READER_TOPIC))
		self.data_received = msg.data

	#^ #######################################
	def _test_base(self):
		self.data_received = None
		timeout = rospy.get_time() + self.timeout
		while not rospy.is_shutdown() and rospy.get_time() < timeout and self.data_received is None:
			rospy.sleep(self.loop_delay)

	#^ #######################################
	def test_if_publishing(self):
		self._test_base()
		string = "Timeout: no messages received on {} for a duration of {} s".format(
			VELOCITY_READER_TOPIC, self.timeout)
		self.assertTrue(self.data_received is not None, string)

	#^ #######################################
	def test_published_values(self):
		"""Check if correct angular velocity is published"""
		self._test_base()
		msg = rospy.wait_for_message(GAZEBO_LINK_STATES_TOPIC, LinkStates, timeout=self.timeout)
		i_l = msg.name.index('trolley::left_wheel_link')
		i_r = msg.name.index('trolley::right_wheel_link')
		gazebo_ang_vel_l = msg.twist[i_l].angular.y
		gazebo_ang_vel_r = msg.twist[i_r].angular.y
		gazebo_ang_vel = (gazebo_ang_vel_l + gazebo_ang_vel_r) / 2.

		string = "Velocity from gazebo={}; Velocity from velocity reader node={}".format(
			gazebo_ang_vel, self.data_received)
		self.assertAlmostEqual(self.data_received, gazebo_ang_vel, msg=string)


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest
	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
	rostest.rosrun(PKG, NODE_NAME, VelocityReaderNodeTest)
