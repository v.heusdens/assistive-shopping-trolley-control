#!/usr/bin/env python

"""Unit test for handle_force_reader_node"""

import rospy
import time
import unittest

from std_msgs.msg import Float64

from toolbox.ros import load_config

PKG = 'trolley_tests'
NODE_NAME = 'test_handle_force_reader_node'
SUBSCRIBER_TOPIC = '/trolley/sensors/handle_force'


#^ CLASS #######################################
class TestHandleForceReaderNode(unittest.TestCase):

	def setUp(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_general = load_config('trolley_general')
		config_tests = load_config('trolley_tests')

		self.loop_delay = config_general['loop_delay']
		self.timeout = config_tests['timeout']
		self.success = False

		rospy.loginfo('Timeout set to {} s'.format(self.timeout), logger_name=NODE_NAME)

		#^ init publishers/subscribers/services
		rospy.Subscriber(SUBSCRIBER_TOPIC, Float64, self.callback)
		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def callback(self, msg):
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC))
		self.success = True

	#^ #######################################
	def test_if_publishing(self):
		timeout = time.time() + self.timeout
		while not rospy.is_shutdown() and time.time() < timeout and not self.success:
			time.sleep(self.loop_delay)

		string = "Timeout: no messages received on {} for a duration of {} s".format(
			SUBSCRIBER_TOPIC, self.timeout)
		self.assertTrue(self.success, string)


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest
	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
	rostest.rosrun(PKG, NODE_NAME, TestHandleForceReaderNode)
