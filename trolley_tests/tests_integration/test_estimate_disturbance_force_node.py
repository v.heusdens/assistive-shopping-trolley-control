#!/usr/bin/env python

"""Unit test for checking estimated disturbance force"""

import rospy
import unittest

from std_msgs.msg import Float64
from trolley_msgs.srv import SetHandleForce

from toolbox.compute import isclose, mean
from toolbox.ros import hold_ros, load_config

PKG = 'trolley_tests'
NODE_NAME = 'test_estimate_disturbance_force_node'
SUBSCRIBER_TOPIC = '/trolley/controller/disturbance_estimate_unfiltered'
SET_HANDLE_FORCE_SERVICE = '/trolley/tests/set_handle_force'
SET_HANDLE_FORCE_ESTIMATE_SERVICE = '/trolley/tests/set_handle_force_estimate'


#^ CLASS #######################################
class TestEstimateDisturbanceForceNode(unittest.TestCase):

	def setUp(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_general = load_config('trolley_general')
		config_controller = load_config('trolley_controller')
		config_tests = load_config('trolley_tests')

		timeout = config_tests['timeout']
		self.loop_delay = config_general['loop_delay']
		self.duration = config_tests['gather_duration']
		self.min_msg_amount = config_controller[
			'update_rate']  #@ Minimum amount of messages that should be received (1 s)

		self.trigger_message_received = False
		self.trigger_gather_data = False

		self.data_estimate = []

		rospy.loginfo('Timeout set to {} s'.format(timeout), logger_name=NODE_NAME)

		#^ init publishers/subscribers/services
		rospy.Subscriber(SUBSCRIBER_TOPIC, Float64, self.callback)
		rospy.wait_for_service(SET_HANDLE_FORCE_SERVICE, timeout=timeout)
		self.set_handle_force_srv = rospy.ServiceProxy(SET_HANDLE_FORCE_SERVICE, SetHandleForce)
		rospy.wait_for_service(SET_HANDLE_FORCE_ESTIMATE_SERVICE, timeout=timeout)
		self.set_handle_force_est_srv = rospy.ServiceProxy(SET_HANDLE_FORCE_ESTIMATE_SERVICE, SetHandleForce)

		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Service client initiated for {}'.format(SET_HANDLE_FORCE_SERVICE),
			logger_name=NODE_NAME)
		rospy.loginfo('Service client initiated for {}'.format(SET_HANDLE_FORCE_ESTIMATE_SERVICE),
			logger_name=NODE_NAME)

	#^ #######################################
	def hold_before_data_gathering(self):
		hold_ros(0.1, self.loop_delay, NODE_NAME)

	def hold_during_data_gathering(self):
		hold_ros(self.duration, self.loop_delay, NODE_NAME)

	#^ #######################################
	def reset(self):
		self.trigger_gather_data = False
		self.data_estimate = []

	#^ #######################################
	def callback(self, msg):
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)
		self.trigger_message_received = True

		if self.trigger_gather_data:
			self.data_estimate.append(msg.data)

	#^ TESTS #######################################
	def _test_arrange(self, handle_force_input, handle_force_est_input):  #% setup of variables
		self.reset()

		#^ set inputs
		self.set_handle_force_srv(handle_force_input)
		self.set_handle_force_est_srv(handle_force_est_input)

		#^ wait
		self.hold_before_data_gathering()

	#^ #######################################
	def _test_act(self):  #% use the function/class and get its result
		rospy.loginfo_once('Gathering data...', logger_name=NODE_NAME)

		self.trigger_gather_data = True
		self.hold_during_data_gathering()
		self.trigger_gather_data = False

	#^ #######################################
	def _check_if_enough_data_received(self):
		if len(self.data_estimate) < self.min_msg_amount:
			string = "Did not receive enough data. Amount: {}".format(len(self.data_estimate))
			rospy.logerr(string, logger_name=NODE_NAME)
			self.fail(string)

	def _check_node_output(self, expected, measured):
		string = 'Expected estimated disturbance value: {:.5f} N \n\
				Average measured estimated disturbance value: {:.5f} N \n\
				Difference: {} N'.format(expected, measured, expected - measured)

		rospy.loginfo('TEST RESULT: {}'.format(string), logger_name=NODE_NAME)
		self.assertAlmostEqual(expected, measured)

	#^ #######################################
	def test_zero_inputs(self):
		#% Arrange: setup of variables
		self._test_arrange(handle_force_input=0, handle_force_est_input=0)

		#% Act: use the function/class and get its result
		self._test_act()

		#% Assert: check if the result is correct
		self._check_if_enough_data_received()

		disturbance_expected = 0
		disturbance_measured_avg = mean(self.data_estimate)
		self._check_node_output(expected=disturbance_expected, measured=disturbance_measured_avg)

	#^ #######################################
	def test_nonzero_equal_inputs(self):
		#% Arrange: setup of variables
		self._test_arrange(handle_force_input=7.5, handle_force_est_input=7.5)

		#% Act: use the function/class and get its result
		self._test_act()

		#% Assert: check if the result is correct
		self._check_if_enough_data_received()

		disturbance_expected = 0
		disturbance_measured_avg = mean(self.data_estimate)
		self._check_node_output(expected=disturbance_expected, measured=disturbance_measured_avg)

	#^ #######################################
	def test_estimate_larger_than_measured(self):
		#% Arrange: setup of variables
		self._test_arrange(handle_force_input=2.5, handle_force_est_input=7.5)

		#% Act: use the function/class and get its result
		self._test_act()

		#% Assert: check if the result is correct
		self._check_if_enough_data_received()

		disturbance_expected = 5
		disturbance_measured_avg = mean(self.data_estimate)
		self._check_node_output(expected=disturbance_expected, measured=disturbance_measured_avg)

	#^ #######################################
	def test_estimate_smaller_than_measured(self):
		#% Arrange: setup of variables
		self._test_arrange(handle_force_input=8, handle_force_est_input=5.5)

		#% Act: use the function/class and get its result
		self._test_act()

		#% Assert: check if the result is correct
		self._check_if_enough_data_received()

		disturbance_expected = -2.5
		disturbance_measured_avg = mean(self.data_estimate)
		self._check_node_output(expected=disturbance_expected, measured=disturbance_measured_avg)


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest

	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
	rostest.rosrun(PKG, NODE_NAME, TestEstimateDisturbanceForceNode)
