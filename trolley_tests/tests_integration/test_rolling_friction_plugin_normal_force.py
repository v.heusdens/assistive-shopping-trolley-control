#!/usr/bin/env python

"""Unit test for checking measured normal force in joint_friction plugin"""

import rospy
import unittest

from trolley_msgs.msg import RollingFrictionTestMsg
from trolley_msgs.srv import SetFrictionCoefficient

from toolbox.params import ParamsNumeric
from toolbox.ros import hold_ros, load_config, wait_on_simulation_start
from toolbox.transform import convert_string_to_sympy

PKG = 'trolley_tests'
NODE_NAME = 'test_rolling_friction_plugin_normal_force'
SUBSCRIBER_TOPIC = '/trolley/tests/rolling_friction_plugin_test_msgs'
SERVICE_NAME = '/trolley/tests/set_simulation_friction_coefficient'


#^ BASE CLASS #######################################
class Base(unittest.TestCase):

	def _setUp(self):
		#^ init params
		self.margin = 0.005  #@ relative to normal_force_calc value

		self.loop_delay = self.config_general['loop_delay']
		self.duration = self.config_tests['gather_duration']
		self.timeout = self.config_tests['timeout']

		self.velocity_deadband = self.config_general['velocity_deadband']
		self.model = self.config_tests['model_Fn']

		self._trigger_message_received = False
		self._trigger_gather_data = False
		self._trigger_deadband = False

		self._data_force = []

		rospy.loginfo('Timeout set to {} s'.format(self.timeout), logger_name=NODE_NAME)

		#^ init publishers/subscribers/services
		rospy.Subscriber(SUBSCRIBER_TOPIC, RollingFrictionTestMsg, self._callback)
		rospy.wait_for_service(SERVICE_NAME, self.timeout)
		self.set_mu_srv = rospy.ServiceProxy(SERVICE_NAME, SetFrictionCoefficient)

		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Service client initiated for {}'.format(SERVICE_NAME), logger_name=NODE_NAME)

	#^ #######################################
	def _wait_on_simulation_start(self):
		"""Function which terminates as soon as simulation time starts running"""
		wait_on_simulation_start(self.loop_delay, self.timeout, NODE_NAME)

	def _hold_before_data_gathering(self):
		if self.gather_delay > 0:
			hold_ros(self.gather_delay + 0.5, self.loop_delay, NODE_NAME)

	def _hold_during_data_gathering(self):
		hold_ros(self.duration, self.loop_delay, NODE_NAME)

	def load_config_files(self):
		self.config_general = load_config('trolley_general')
		self.config_tests = load_config('trolley_tests')

	#^ #######################################
	def _callback(self, msg):
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)
		self._trigger_message_received = True

		if self._trigger_gather_data:
			rospy.loginfo_once('Gathering data...', logger_name=NODE_NAME)
			self._data_force.append(msg.normal_force)

			#^ Velocity within deadband when nonzero velocity is specified
			if self.model_params['d_xw'] != 0 and abs(msg.wheel_angular_velocity) <= self.velocity_deadband:
				self._trigger_deadband = True
				rospy.logwarn_throttle(1,
					'Trolley velocity within velocity deadband: rolling friction will be 0',
					logger_name=NODE_NAME)

	#^ #######################################
	def set_mu(self, value):
		self.model_params['mu'] = value

		_success = self.set_mu_srv(value)
		if not _success:
			rospy.logerr('ERROR: set_simulation_friction_coefficient service call failed', logger_name=NODE_NAME)
			return

		rospy.loginfo('Friction coefficient succesfully set to {}'.format(value), logger_name=NODE_NAME)

	#^ #######################################
	def _calc_normal_force(self, mu):
		p = ParamsNumeric()
		p.set_params_to_type(['L_h', 'm_b'], 'nom')

		p.d_xw = self.model_params['d_xw']
		p.mu = mu
		p.F_hx = self.model_params['F_hx']
		p.T_m = self.model_params['T_m']

		p.d_beta = 0
		p.gamma = 0

		result = convert_string_to_sympy(self.model, p)
		rospy.loginfo('Vars set in model: d_xw={}; F_hx={}; T_m={}; m_ft={}; R_w={}; gamma={}; mu={}'.format(
			p.d_xw, p.F_hx, p.T_m, p.m_ft, p.R_w, p.gamma, p.mu),
			logger_name=NODE_NAME)

		return result

	#^ #######################################
	def _test_normal_force(self):
		#^ wait for simulation
		self._wait_on_simulation_start()
		self._hold_before_data_gathering()

		#^ gather data
		self._trigger_gather_data = True
		self._hold_during_data_gathering()
		self._trigger_gather_data = False

		#^ test data
		#@ No info on topic
		if not self._trigger_message_received:
			_string = "Timeout: no messages received on {} for a duration of {} s".format(SUBSCRIBER_TOPIC, self.duration)
			self.assertTrue(False, _string)
			return

		#@ Velocity has been within deadband when nonzero velocity is specified
		if self._trigger_deadband:
			self.assertTrue(False, 'Trolley velocity has been within velocity deadband')

		#@ Proper data received
		_normal_force_calc = self._calc_normal_force(self.model_params['mu'])
		_upper_bound = _normal_force_calc * (1 + self.margin)
		_lower_bound = _normal_force_calc * (1 - self.margin)

		_data_avg = sum(self._data_force) / len(self._data_force)
		_data_error = (_data_avg - _normal_force_calc) / _normal_force_calc

		_string = 'Calculated normal force value for mu = {:.1f}: {:.5f} \n	Acceptable range:({:.5f}, {:.5f}) ({}% margin) \n	Average measured normal force value: {:.5f} ({:.2f}% off)'.format(
			self.model_params['mu'], _normal_force_calc, _lower_bound, _upper_bound, self.margin * 100, _data_avg,
			_data_error * 100)

		rospy.loginfo('\nTEST RESULT:\n' + _string, logger_name=NODE_NAME)
		#rospy.logwarn('\nDATA:\n{}'.format(self._data_force), logger_name=NODE_NAME)

		self.assertTrue(_lower_bound <= _data_avg <= _upper_bound, _string)


#^ SUPERCLASS NO VELOCITY #######################################
class ZeroVelocity(Base):

	def setUp(self):
		rospy.loginfo("Running ZeroVelocity tests", logger_name=NODE_NAME)
		self.load_config_files()
		self.gather_delay = 0.25
		self.model_params = {
			'd_xw': 0,
			'F_hx': 0,
			'T_m': 0,
		}
		self._setUp()

	def test_zero_velocity_without_friction(self):
		self.set_mu(0)
		self._test_normal_force()

	def test_zero_velocity_with_friction(self):
		self.set_mu(self.config_tests['friction_coeff'])
		self._test_normal_force()


#^ SUPERCLASS NO INPUT #######################################
class ZeroInput(Base):
	#@ Does not take into account additional support wheel mass

	def setUp(self):
		rospy.loginfo("Running ZeroInput tests", logger_name=NODE_NAME)
		self.load_config_files()
		self.gather_delay = rospy.get_param('~acc_duration')
		self.model_params = {
			'd_xw': 1,
			'F_hx': 0,
			'T_m': 0,
		}
		self._setUp()

	def test_normal_force_nonzero_velocity_no_input_without_friction(self):
		self.set_mu(0)
		self._test_normal_force()

	def test_normal_force_nonzero_velocity_no_input_with_friction(self):
		self.set_mu(self.config_tests['friction_coeff'])
		self._test_normal_force()


#^ SUPERCLASS FORCE INPUT #######################################
class ForceInput(Base):
	#@ Does not take into account additional support wheel mass

	def setUp(self):
		rospy.loginfo("Running ForceInput tests", logger_name=NODE_NAME)
		self.load_config_files()
		self.gather_delay = 0.25
		self.model_params = {
			'd_xw': 1,
			'F_hx': self.config_tests['input_force'],
			'T_m': 0,
		}
		self._setUp()

	def test_normal_force_nonzero_velocity_with_input_force_without_friction(self):
		self.set_mu(0)
		self._test_normal_force()

	def test_normal_force_nonzero_velocity_with_input_force_with_friction(self):
		self.set_mu(self.config_tests['friction_coeff'])
		self._test_normal_force()


#^ SUPERCLASS TORQUE INPUT #######################################
class TorqueInput(Base):
	#@ Does not take into account additional support wheel mass

	def setUp(self):
		rospy.loginfo("Running torque input tests", logger_name=NODE_NAME)
		self.load_config_files()
		self.gather_delay = 0.25
		self.model_params = {
			'd_xw': 1,
			'F_hx': 0,
			'T_m': self.config_tests['motor_torque'],
		}
		self._setUp()

	def test_normal_force_nonzero_velocity_with_input_torque_without_friction(self):
		self.set_mu(0)
		self._test_normal_force()

	def test_normal_force_nonzero_velocity_with_input_torque_with_friction(self):
		self.set_mu(self.config_tests['friction_coeff'])
		self._test_normal_force()


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest

	rospy.init_node(NODE_NAME)
	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)

	#^ Get info from param server
	_d_xw = rospy.get_param('~velocity')
	_F_hx = rospy.get_param('~force')
	_T_m = rospy.get_param('~torque')

	#^ Decide which test to run
	if _T_m:
		test_case = TorqueInput
	elif _F_hx:
		test_case = ForceInput
	elif _d_xw:
		test_case = ZeroInput
	else:
		test_case = ZeroVelocity

	#^ Run test
	rostest.rosrun(PKG, NODE_NAME, test_case)
