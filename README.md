This code accompanies the thesis work:

**"Assistive Shopping Trolley Controller Design For the Elderly"**

presented for the degree of Master of Science in Mechanical Engineering at the Delft University of Technology, by Vianne Heusdens.  

The thesis report can be obtained from the [TU Delft Education Repository](https://repository.tudelft.nl/islandora/search/?collection=education).  
In order to run the code, ROS needs to be installed and included in the Python path. See [ROS installation instructions](http://wiki.ros.org/ROS/Installation).
