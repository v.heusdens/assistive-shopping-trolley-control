#!/usr/bin/env python

"""Node that reads force from handle force sensor and publishes the force along the road on the '\trolley\handle_force' topic."""
import rospy
from rospy.exceptions import ROSException

from geometry_msgs.msg import Vector3
from std_msgs.msg import Float64

PKG = 'trolley_sensors'
NODE_NAME = 'handle_force_reader_node'
PUBLISHER_TOPIC = '/trolley/sensors/handle_force'
SUBSCRIBER_TOPIC = '/trolley/plugins/handle_force_measurement'


#^ CLASS #######################################
class HandleForceReaderNode():

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Float64, queue_size=1)
		rospy.Subscriber(SUBSCRIBER_TOPIC, Vector3, self._callback)

		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def _callback(self, msg):
		"""Relay incoming force messages to sensor output topic"""
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)
		try:
			self.pub.publish(msg.x)
		except ROSException as e:
			rospy.logwarn(e)

	#^ #######################################
	def spin(self):
		"""Main loop"""
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		while not rospy.is_shutdown():
			rospy.spin()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = HandleForceReaderNode()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
