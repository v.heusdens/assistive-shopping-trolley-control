#!/usr/bin/env python

"""Node that reads acceleration from IMU sensor and publishes the linear forward acceleration on the '\trolley\acceleration' topic."""

import rospy
from rospy.exceptions import ROSException

from sensor_msgs.msg import Imu
from std_msgs.msg import Float64

PKG = 'trolley_sensors'
NODE_NAME = 'imu_reader_node'
PUBLISHER_TOPIC = '/trolley/sensors/acceleration'
SUBSCRIBER_TOPIC = '/trolley/plugins/imu_measurement'


#^ CLASS #######################################
class ImuReaderNode():

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Float64, queue_size=1)
		rospy.Subscriber(SUBSCRIBER_TOPIC, Imu, self._callback)

		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def _callback(self, msg):
		"""Relay linear acceleration from IMU message to sensor topic"""
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)
		try:
			self.pub.publish(msg.linear_acceleration.x)
		except ROSException as e:
			rospy.logwarn(e)

	#^ #######################################
	def spin(self):
		"""Main loop"""
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		while not rospy.is_shutdown():
			rospy.spin()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = ImuReaderNode()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
