#!/usr/bin/env python

"""Node that reads link state data from /gazebo/link_states topic and publishes the angular wheel velocity on the '\trolley\velocity' topic."""

import rospy

from gazebo_msgs.msg import LinkStates
from std_msgs.msg import Float64

from toolbox.ros import load_config, wait_on_simulation_start

PKG = 'trolley_sensors'
NODE_NAME = 'velocity_reader_node'
PUBLISHER_TOPIC = '/trolley/sensors/velocity'
SUBSCRIBER_TOPIC = '/gazebo/link_states'


#^ CLASS #######################################
class VelocityReaderNode():

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_general = load_config('trolley_general')
		config_model = load_config('trolley_description')

		self.loop_delay = config_general['loop_delay']
		self.R_w = config_model['wheel_radius']

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Float64, queue_size=1)
		rospy.Subscriber(SUBSCRIBER_TOPIC, LinkStates, self._callback)

		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def _callback(self, msg):
		"""Calculate average angular velocity between wheels, publish result on sensor topic"""
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

		try:
			#^ Obtain index of target links
			i_l = msg.name.index('trolley::left_wheel_link')
			i_r = msg.name.index('trolley::right_wheel_link')
		except ValueError:
			if rospy.get_time() < 0.5:  #@ prevent useless error messages
				return
			else:
				raise

		#^ Obtain and combine velocity measurements
		ang_vel_l = msg.twist[i_l].angular.y
		ang_vel_r = msg.twist[i_r].angular.y
		ang_vel = (ang_vel_l + ang_vel_r) / 2.
		#lin_vel = ang_vel * self.R_w

		#^ Publish
		try:
			self.pub.publish(ang_vel)
		except rospy.ROSException as e:
			rospy.logwarn('ROSException occurred: {}'.format(e), logger_name=NODE_NAME)

	#^ #######################################
	def spin(self):
		"""Main loop"""
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		while not rospy.is_shutdown():
			rospy.spin()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = VelocityReaderNode()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
