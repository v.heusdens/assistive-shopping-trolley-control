#!/usr/bin/env python

"""This script posts simulation-data to the /trolley/handle_force_measurement topic"""

import rospy

from geometry_msgs.msg import Vector3
from trolley_msgs.srv import EmptyTrigger, EmptyTriggerResponse, GetGeneratorForce

from toolbox.params import ParamsNumeric
from toolbox.ros import hold_ros, load_config, wait_on_simulation_start
from toolbox.transform import convert_string_to_sympy

PKG = 'trolley_sensors'
NODE_NAME = 'handle_force_generator_node'
PUBLISHER_TOPIC = '/trolley/plugins/handle_force_measurement'
SERVICE_TOPIC = '/trolley/tests/get_generator_handle_force'


#^ CLASS #######################################
class HandleForceGeneratorNode():
	#^ #######################################
	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_general = load_config('trolley_general')
		config_description = load_config('trolley_description')
		config_sensors = load_config('trolley_sensors')
		config_simulation = load_config('trolley_simulation')
		config_controller = load_config('trolley_controller')

		self.loop_delay = config_general['loop_delay']
		self.rate = config_sensors['update_rate']
		config_simulation = load_config('trolley_simulation')

		target_vel = config_simulation['target_velocity']
		self.support_wheel_mass_ratio = config_simulation['support_wheel_mass_ratio']
		self.acc_duration = config_simulation['acceleration_duration']
		self.publish_delay = config_sensors['publish_delay']
		self.timeout = False

		if rospy.has_param('~added_mass'):
			added_mass_type = rospy.get_param('~added_mass')
		else:
			added_mass_type = False

		if added_mass_type is False:
			added_mass_acc = 0
			added_mass_ss = 0
		elif added_mass_type == 'acc':
			added_mass_acc = config_description['bag_mass_added']
			added_mass_ss = 0
		elif added_mass_type == 'ss':
			added_mass_acc = 0
			added_mass_ss = config_description['bag_mass_added']
		elif added_mass_type is True or added_mass_type == 'both':
			added_mass_acc = config_description['bag_mass_added']
			added_mass_ss = config_description['bag_mass_added']
		else:
			raise Exception(
			    "Value '{}' for param 'added_mass' incorrect. Please use True, False, 'acc', 'ss' or 'both'.".
			    format(added_mass_type)
			)

		self.model = config_controller['model_Fhx']

		self.acc_force = self.calculate_force(target_vel, self.acc_duration, added_mass_acc)
		rospy.loginfo('acceleration force set to {} N'.format(self.acc_force), logger_name=NODE_NAME)

		self.ss_force = self.calculate_force(target_vel, None, added_mass_ss)
		rospy.loginfo('steady-state force set to {} N'.format(self.ss_force), logger_name=NODE_NAME)

		#^ init msg
		self.msg = Vector3()
		self.msg.y = 0
		self.msg.z = 0

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Vector3, queue_size=1)
		rospy.Service(SERVICE_TOPIC, GetGeneratorForce, self.get_force_srv_handle)

		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Service initiated for {}'.format(SERVICE_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def wait_on_simulation_start(self):
		"""Function which terminates as soon as simulation time starts running"""
		wait_on_simulation_start(self.loop_delay, self.timeout, NODE_NAME)

	def wait_before_publishing(self):
		"""Hold some time before start publishing on output topic"""
		hold_ros(self.publish_delay, self.loop_delay, NODE_NAME)

	#^ #######################################
	def get_force_srv_handle(self, req):
		"""Returns calculated handle force. Types are either 'acc' for acceleration force or 'ss' for steady_state force"""
		if req.type == 'acc':  #^ return acceleration force
			rospy.loginfo('Acceleration force requested. Return value {}'.format(self.acc_force), logger_name=NODE_NAME)
			return self.acc_force
		elif req.type == 'ss':  #^ return steady-state force
			rospy.loginfo('Steady-state force requested. Return value {}'.format(self.ss_force), logger_name=NODE_NAME)
			return self.ss_force

	#^ #######################################
	def calculate_force(self, target_vel, acc_duration, added_mass=0):
		"""Calculates desired force for obtaining target velocity after a certain time"""
		p = ParamsNumeric()
		p.set_params_to_type(['L_h', 'mu', 'm_b', 'gamma'], 'nom')
		p.d_xw = 1
		p.d_beta = 0
		p.T_m = 0
		p.gamma = 0
		p.m_w = p.m_w * (1 + self.support_wheel_mass_ratio)
		p.m_b += added_mass

		if acc_duration is None:
			p.dd_xw = 0
		else:
			p.dd_xw = target_vel / acc_duration
		rospy.loginfo(
		    'Vars set in model:\n \
			d_xw={}; dd_xw={}; mu={}; T_m={}; m_ft={}; m_w={}; R_w={}; m_acc={}; gamma={}'.format(
		        p.d_xw, p.dd_xw, p.mu, p.T_m, p.m_ft, p.m_w, p.R_w, p.m_acc, p.gamma
		    ),
		    logger_name=NODE_NAME
		)

		force = convert_string_to_sympy(self.model, p)
		return force

	#^ #######################################
	def publish(self):
		"""Determine which force to publish & publish"""
		rate = rospy.Rate(self.rate)
		acc_timeout = rospy.get_time() + self.acc_duration

		while not rospy.is_shutdown():
			rospy.loginfo_once('Started publishing on topic {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)

			if rospy.get_time() < acc_timeout:
				self.msg.x = self.acc_force
				rospy.loginfo_once(
				    'Publishing acceleration force of {} N for {} seconds'.format(self.acc_force, self.acc_duration),
				    logger_name=NODE_NAME
				)
			else:
				self.msg.x = self.ss_force
				rospy.loginfo_once('Publishing steady-state force of {} N'.format(self.ss_force), logger_name=NODE_NAME)

			self.pub.publish(self.msg)

			rate.sleep()

	#^ #######################################
	def spin(self):
		"""Main loop"""
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		self.wait_on_simulation_start()
		self.wait_before_publishing()
		self.publish()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = HandleForceGeneratorNode()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
