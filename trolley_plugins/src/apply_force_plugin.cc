""
	"Plugin for applying force on a point defined in a local reference frame."
	""

#include <functional>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>

#include "geometry_msgs/Vector3.h"

#include <ignition/math/Vector3.hh>

#include <ros/ros.h>
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"

	namespace gazebo
{
	class ApplyForce : public ModelPlugin
	{
		//#//////////////////////////////////////////////////////
	public:
		void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
		{
			ROS_INFO("-- LOADING APPLYFORCE PLUGIN --");

			//% Store the pointer to the model
			this->model_ = _parent;
			ROS_INFO("parent is: %s", this->model_->GetName().c_str());

			//% Get pointer to link which' local reference frame should be used for force definition
			if (!_sdf->HasElement("link"))
			{
				ROS_ERROR("No target link specified. ApplyForce plugin could not be loaded.");
				return;
			}
			std::string link_name_ = _sdf->GetElement("link")->Get<std::string>();
			this->link_ = this->model_->GetLink(link_name_);
			if (this->link_)
			{
				ROS_INFO("ApplyForce plugin loaded on link: %s", link_name_.c_str());
			}
			else
				ROS_ERROR_STREAM("Link '" << link_name_ << "' not found! ApplyForce plugin could not be loaded.");

			//% Check and store position at which force should be applied
			if (!_sdf->HasElement("x") or !_sdf->HasElement("y") or !_sdf->HasElement("z"))
			{
				ROS_WARN("No application point specified. Using local reference frame origin");
			}
			else
			{
				std::string temp_x = _sdf->GetElement("x")->Get<std::string>();
				std::string temp_y = _sdf->GetElement("y")->Get<std::string>();
				std::string temp_z = _sdf->GetElement("z")->Get<std::string>();
				float temp_x2 = strtof((temp_x).c_str(), 0);
				float temp_y2 = strtof((temp_y).c_str(), 0);
				float temp_z2 = strtof((temp_z).c_str(), 0);
				this->pos = ignition::math::Vector3d(temp_x2, temp_y2, temp_z2);
				ROS_INFO("Set application point to...x = %f, y = %f, z = %f w.r.t local reference frame", temp_x2, temp_y2, temp_z2);
			}

			//% Listen to Gazebo's update event. This event is broadcast every simulation iteration.
			this->updateConnection_ = event::Events::ConnectWorldUpdateBegin(
				std::bind(&ApplyForce::OnUpdate, this));

			//& ROS
			//% Initialize ros, if it has not already bee initialized.
			if (!ros::isInitialized())
			{
				int argc = 0;
				char **argv = NULL;
				ros::init(argc, argv, "apply_force_plugin_node",
						  ros::init_options::NoSigintHandler);
				ROS_INFO_STREAM("ROS initialized");
			}

			//% Create ROS node
			this->rosNode.reset(new ros::NodeHandle("apply_force_plugin_node"));

			//% Determine subscriber topic
			if (!_sdf->HasElement("topic"))
			{
				this->topic = "/trolley/plugins/handle_force_measurement";
				ROS_WARN("No topic specified. Using default topic: %s", this->topic.c_str());
			}
			else
			{
				this->topic = _sdf->GetElement("topic")->Get<std::string>();
				ROS_INFO("Set subscriber topic to...%s", this->topic.c_str());
			}

			//% Initialize subscriber
			this->rosSub = this->rosNode->subscribe<geometry_msgs::Vector3>(this->topic, 1, &ApplyForce::OnRosMsg, this);
			ROS_INFO("ApplyForce plugin loaded!");
		}

		//%//////////////////////////////////////////////////////
		//% Called by the world update start event
		void OnUpdate()
		{
			//@ Apply a force to the model in its own reference frame
			this->link_->AddLinkForce(this->force, this->pos);
		}

		//%//////////////////////////////////////////////////////
		//% Subscriber callback
		void OnRosMsg(const geometry_msgs::Vector3ConstPtr &_msg)
		{
			//@ Store force
			this->force = ignition::math::Vector3d(_msg->x, _msg->y, _msg->z);
		}

		//#//////////////////////////////////////////////////////
		//# Parameters
	private:
		std::string topic;						  //@ Topic name
		ignition::math::Vector3d force;			  //@ Store most recent force value
		ignition::math::Vector3d pos;			  //@ Force application position
		physics::LinkPtr link_;					  //@ Pointers to the joints
		physics::ModelPtr model_;				  //@ Pointer to the model
		event::ConnectionPtr updateConnection_;	  //@ Pointer to the update event connection
		std::unique_ptr<ros::NodeHandle> rosNode; //@ A node used for ROS transport
		ros::Subscriber rosSub;					  //@ A ROS subscriber
	};

	//# Register this plugin with the Gazebo simulator
	GZ_REGISTER_MODEL_PLUGIN(ApplyForce)
} // namespace gazebo
