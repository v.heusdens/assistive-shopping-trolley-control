""
	"Plugin for implementation of custom rolling friction dependent on normal force and acting only on wheel links"
	""

#include <functional>

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ros/ros.h>
#include <ros/package.h>

#include <ignition/math/Vector3.hh>
#include <std_msgs/Float64.h>
#include <trolley_msgs/SetFrictionCoefficient.h>
#include <trolley_msgs/RollingFrictionTestMsg.h>

#include "yaml-cpp/yaml.h"

	namespace gazebo
{
	float calculate_sign_with_deadband(float value, float deadband)
	{
		// Return the sign of value, but return 0 if absolute(value) < deadband
		if (value > deadband)
			return 1.;
		else if (value < -deadband)
			return -1.;
		return 0.;
	}

	class RollingFriction : public ModelPlugin
	{
		//#//////////////////////////////////////////////////////
	public:
		void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
		{
			ROS_INFO("-- LOADING ROLLINGFRICTION PLUGIN --");

			//% Store the pointer to the model
			this->_model = _parent;
			ROS_INFO("parent is: %s", this->_model->GetName().c_str());

#pragma region LOAD PARAMS
			//% Config files
			//@ Load files
			std::string config_general_path = ros::package::getPath("toolbox") + "/../config/config_general.yaml";
			YAML::Node config_general = YAML::LoadFile(config_general_path.c_str());

			std::string config_test_path = ros::package::getPath("trolley_tests") + "/config/config_tests.yaml";
			YAML::Node config_tests = YAML::LoadFile(config_test_path.c_str());

			std::string config_description_path = ros::package::getPath("trolley_description") + "/config/config_description.yaml";
			YAML::Node config_description = YAML::LoadFile(config_description_path.c_str());

			//@ Set params
			this->wheel_radius = config_description["wheel_radius"].as<float>();
			this->gravity = config_description["gravity"].as<float>();
			this->deadband = config_general["velocity_deadband"].as<float>();
			ROS_INFO("Velocity deadband set to %f rad/s", this->deadband);

			//% Parameter server
			//@ friction coefficient value
			if (!_sdf->HasElement("mu"))
			{
				ROS_ERROR("friction coefficient 'mu' missing. RollingFriction plugin could not be loaded");
				return;
			}
			else
			{
				this->mu = _sdf->GetElement("mu")->Get<float>();
				ROS_INFO("Friction coefficient set to %f", this->mu);
			}

			//@ wheel mass
			if (!_sdf->HasElement("wheel_mass"))
			{
				ROS_ERROR("wheel mass 'wheel_mass' missing. RollingFriction plugin could not be loaded");
				return;
			}
			else
			{
				this->wheel_mass = _sdf->GetElement("wheel_mass")->Get<float>();
				ROS_INFO("Wheel mass set to %f", this->wheel_mass);
			}

			//@ unit-test trigger
			if (_sdf->HasElement("enable_test"))
			{
				this->enable_test = _sdf->GetElement("enable_test")->Get<bool>();
				if (this->enable_test)
					ROS_INFO("Test-mode triggered");
			}

#pragma endregion

#pragma region LINK / JOINT POINTERS
			if ((!_sdf->HasElement("target1")) || (!_sdf->HasElement("target2")))
			{
				ROS_ERROR("'target1' or 'target2' specification missing. RollingFriction plugin could not be loaded");
				return;
			}
			std::string target1_name = _sdf->GetElement("target1")->Get<std::string>();
			std::string target2_name = _sdf->GetElement("target2")->Get<std::string>();

			this->_link1 = this->_model->GetLink(target1_name + "_link");
			this->_link2 = this->_model->GetLink(target2_name + "_link");
			this->_joint1 = this->_model->GetJoint(target1_name + "_joint");
			this->_joint2 = this->_model->GetJoint(target2_name + "_joint");

			std::vector<std::string> missing;
			if (!this->_link1)
				missing.push_back(this->_link1->GetName());
			if (!this->_link2)
				missing.push_back(this->_link2->GetName());
			if (!this->_joint1)
				missing.push_back(this->_joint1->GetName());
			if (!this->_joint2)
				missing.push_back(this->_joint2->GetName());

			if (!missing.empty())
			{
				for (std::string missing_elem : missing)
					ROS_ERROR("The following element does not exist in model %s: %s", this->_model->GetName().c_str(), missing_elem.c_str());
				return;
			}
			else
				ROS_INFO("RollingFriction plugin loading on links %s & %s", target1_name.c_str(), target2_name.c_str());
#pragma endregion

#pragma region TEST ELEMENTS
			if (this->enable_test)
			{
				//% Initialize ros, if it has not already bee initialized.
				if (!ros::isInitialized())
				{
					int argc = 0;
					char **argv = NULL;
					ros::init(argc, argv, "rolling_friction_plugin",
							  ros::init_options::NoSigintHandler);
					ROS_INFO_STREAM("ROS initialized");
				}

				//% Create ROS node
				this->rosNode.reset(new ros::NodeHandle("rolling_friction_plugin"));

				//% Setup publisher
				if (_sdf->HasElement("topic"))
					this->rospub_topic = _sdf->GetElement("topic")->Get<std::string>();
				this->rosPub = this->rosNode->advertise<trolley_msgs::RollingFrictionTestMsg>(this->rospub_topic, 1);

				//% Setup service for changing mu during runtime
				this->rosSrv = this->rosNode->advertiseService(this->rossrv_name, &RollingFriction::ServiceCallback, this);
				ROS_INFO("Service %s initiated", this->rossrv_name.c_str());
			}
#pragma endregion
			//% Listen to the update event. This event is broadcast every simulation iteration.
			this->updateConnection = event::Events::ConnectWorldUpdateBegin(
				std::bind(&RollingFriction::OnUpdate, this));

			ROS_INFO("RollingFriction plugin loaded!");
		}

		//# Functions
		//% Called by the world update start event
		void OnUpdate()
		{
			if (ros::Time::now() > ros::Time(0.2)) // Wait for params values to stabilize
			{
				if (this->trigger_calibration)
					this->CalibrateVelocity();	 // Save velocity measurement offset
				this->GetAngularWheelVelocity(); // Save angular wheel velocity obtained from Gazebo
				this->GetNormalForce();			 // Save normal force obtained from Gazebo
				this->SetRollingFriction();		 // Apply friction torque to targets
												 //this->LogTestData();
			}

			//@ If test enabled, publish test data
			if (enable_test)
				this->PublishTestData();
		}

		void CalibrateVelocity() // Save velocity measurement offset
		{
			this->GetAngularWheelVelocity();					  // Save angular wheel velocity obtained from Gazebo
			this->velocity_offset = this->wheel_angular_velocity; // Save current velocity as offset
			this->trigger_calibration = false;					  // Disable calibration
			ROS_INFO("Rolling friction plugin velocity offset set to %f", this->velocity_offset);
		}

		void SetRollingFriction() // Apply friction torque to targets
		{
			double rolling_friction_value = this->mu * this->normal_force * this->wheel_radius;			   // Calculate rolling friction
			float direction = -calculate_sign_with_deadband(this->wheel_angular_velocity, this->deadband); // Apply deadband
			this->rolling_friction = rolling_friction_value * direction;

			// Apply rolling friction to targets
			ignition::math::Vector3d rolling_friction_vector(0, this->rolling_friction, 0);
			this->_link1->AddRelativeTorque(rolling_friction_vector);
			this->_link2->AddRelativeTorque(rolling_friction_vector);
		}

		void GetNormalForce() // Save normal force obtained from Gazebo
		{
			// Get force applied by parent link on wheel joint, specified in parent link frame
			float wheel_force_1 = this->_joint1->GetForceTorque(0).body1Force.Z();
			float wheel_force_2 = this->_joint2->GetForceTorque(0).body1Force.Z();

			float wheel_force_gravity = this->wheel_mass * this->gravity;					 // Calculate wheel gravity force
			this->normal_force = -(wheel_force_1 + wheel_force_2) / 2 + wheel_force_gravity; // Calculate and save normal force per wheel
		}

		void GetAngularWheelVelocity() // Save angular wheel velocity obtained from Gazebo
		{
			// Get angular velocity per wheel from Gazebo
			ignition::math::Vector3d angular_velocity_vector_1 = this->_link1->RelativeAngularVel();
			ignition::math::Vector3d angular_velocity_vector_2 = this->_link2->RelativeAngularVel();
			double angular_velocity_1 = angular_velocity_vector_1.Y() - this->velocity_offset;
			double angular_velocity_2 = angular_velocity_vector_2.Y() - this->velocity_offset;
			this->wheel_angular_velocity = (angular_velocity_1 + angular_velocity_2) / 2.; // Calculate and save average
		}

		void PublishTestData() // For test purposes
		{
			trolley_msgs::RollingFrictionTestMsg msg;
			msg.normal_force = this->normal_force;
			msg.rolling_friction_torque = this->rolling_friction;
			msg.wheel_angular_velocity = this->wheel_angular_velocity;
			this->rosPub.publish(msg);
		}

		bool ServiceCallback(trolley_msgs::SetFrictionCoefficient::Request &request, trolley_msgs::SetFrictionCoefficient::Response &response) // For test purposes
		{
			this->mu = request.mu;
			ROS_INFO("Rolling friction plugin: %s service triggered", this->rossrv_name.c_str());
			ROS_INFO("Rolling friction plugin: Friction coefficient set to %f", this->mu);
			response.success = true;
		}

		//# Parameters
	private:
		// Pointers
		physics::ModelPtr _model;
		physics::LinkPtr _link1;
		physics::LinkPtr _link2;
		physics::JointPtr _joint1;
		physics::JointPtr _joint2;

		// Constants
		float gravity;
		float wheel_radius;
		float wheel_mass;
		float mu;		 // Friction coefficient
		double deadband; // Velocity deadband threshold

		// Get from Gazebo simulation
		float normal_force = 0;
		double wheel_angular_velocity = 0;
		double velocity_offset = 0;

		// Get from plugin
		double rolling_friction = 0;
		bool trigger_calibration = true;

		event::ConnectionPtr updateConnection;

		//% Tests
		bool enable_test = false;

		std::unique_ptr<ros::NodeHandle> rosNode;
		ros::Publisher rosPub;
		ros::Subscriber rosSub;
		ros::ServiceServer rosSrv;

		std::string rospub_topic = "/trolley/tests/rolling_friction_plugin_test_msgs";
		std::string rossrv_name = "/trolley/tests/set_simulation_friction_coefficient";
	};

	//# Register this plugin with the simulator
	GZ_REGISTER_MODEL_PLUGIN(RollingFriction)
} // namespace gazebo
