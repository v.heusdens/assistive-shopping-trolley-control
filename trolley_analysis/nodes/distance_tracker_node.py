#!/usr/bin/env python

"""Node that reads trolley wheel center current_position from the '/gazebo/link_states' topic, calculates the traveled distance and publishes it on the '/trolley/analysis/traveled_distance' topic."""

import numpy as np
import rospy

from gazebo_msgs.msg import LinkStates
from geometry_msgs.msg import Point
from std_msgs.msg import Float64
from toolbox.ros import load_config

PKG = 'trolley_analysis'
NODE_NAME = 'distance_tracker_node'
SUBSCRIBER_TOPIC = '/gazebo/link_states'
PUBLISHER_TOPIC = '/trolley/analysis/traveled_distance'


#^ CLASS #######################################
class DistanceTrackerNode():

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_general = load_config('trolley_general')

		self.loop_delay = config_general['loop_delay']
		self.current_position = Point()
		self.traveled_distance = 0

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Float64, queue_size=1)
		rospy.Subscriber(SUBSCRIBER_TOPIC, LinkStates, self._callback)

		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def _callback(self, msg):
		"""Obtains current position, calls function to update position and traveled distance and publishes result on analysis topic."""
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

		try:
			#^ obtain index of target links
			i = msg.name.index('trolley::base_footprint')
		except ValueError:
			if rospy.get_time() < 0.5:  #@ prevent useless error messages
				return
			else:
				raise

		#^ obtain current_position
		new_position = msg.pose[i].position
		self._update_distance(new_position)

		#^ publish
		try:
			self.pub.publish(self.traveled_distance)
		except rospy.ROSException as e:
			if e.message == 'publish() to a closed topic':
				rospy.logwarn(e.message, logger_name=NODE_NAME)
			else:
				raise

	#^ #######################################
	def _update_distance(self, new_position):
		"""Calculates and updates traveled distance."""
		delta_position_x = new_position.x - self.current_position.x
		delta_position_z = new_position.z - self.current_position.z
		delta_distance = np.sqrt(delta_position_x**2 + delta_position_z**2)

		self.traveled_distance += delta_distance
		self.current_position = new_position

	#^ #######################################
	def spin(self):
		"""Main loop"""
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		while not rospy.is_shutdown():
			rospy.spin()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = DistanceTrackerNode()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
