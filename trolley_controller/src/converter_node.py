#!/usr/bin/env python

"""Node that converts filtered disturbance estimation force to motor torque."""

import rospy

from std_msgs.msg import Float64

from toolbox.ros import load_config

PKG = 'trolley_tests'
NODE_NAME = 'converter_node'
SUBSCRIBER_TOPIC = '/trolley/controller/disturbance_estimate_filtered'
PUBLISHER_TOPIC = '/trolley/actuators/motor_torque'


#^ CLASS #######################################
class ConverterNode:

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		model_config = load_config('trolley_description')
		self.R_w = model_config['wheel_radius']  #@ configure wheel radius

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Float64, queue_size=1)
		rospy.Subscriber(SUBSCRIBER_TOPIC, Float64, self._callback)

		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def _callback(self, msg):
		"""Compute motor torque per wheel."""
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

		force = msg.data
		torque_tot = -self.R_w * force  #@ compute total torque
		torque_per_wheel = 0.5 * torque_tot  #@ compute torque per wheel
		self.pub.publish(torque_per_wheel)  #@ publish result on output topic

	#^ #######################################
	def spin(self):
		"""Main loop"""
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		while not rospy.is_shutdown():
			rospy.spin()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = ConverterNode()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
