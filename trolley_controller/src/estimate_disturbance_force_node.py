#!/usr/bin/env python

"""Node that compares estimated and real handle force input and publishes the resulting disturbance estimation."""

import rospy

from std_msgs.msg import Float64

from toolbox.ros import load_config, wait_on_simulation_start

PKG = 'trolley_tests'
NODE_NAME = 'estimate_disturbance_force_node'
FORCE_MEASUREMENT_TOPIC = '/trolley/sensors/handle_force'
FORCE_ESTIMATE_TOPIC = '/trolley/controller/handle_force_estimate'
PUBLISHER_TOPIC = '/trolley/controller/disturbance_estimate_unfiltered'


#^ CLASS #######################################
class EstimateDisturbanceForceNode:

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		#@ load info from config files
		config_general = load_config('trolley_general')
		self.loop_delay = config_general['loop_delay']

		#@ init instances for saving incoming data
		self.handle_force_measurement = 0
		self.handle_force_estimate = 0

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Float64, queue_size=1)
		rospy.Subscriber(FORCE_MEASUREMENT_TOPIC, Float64, self._callback_handle_force_measurement)
		rospy.Subscriber(FORCE_ESTIMATE_TOPIC, Float64, self._callback_handle_force_estimate)

		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(FORCE_MEASUREMENT_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(FORCE_ESTIMATE_TOPIC), logger_name=NODE_NAME)

		#^ other
		self._wait_on_simulation_start()

	#^ #######################################
	def _wait_on_simulation_start(self):
		"""Function which terminates as soon as simulation time starts running"""
		wait_on_simulation_start(self.loop_delay, NODE_NAME=NODE_NAME)

	#^ #######################################
	def _callback_handle_force_measurement(self, msg):
		"""Store handle force measurement."""
		rospy.loginfo_once('Message received on topic {}'.format(FORCE_MEASUREMENT_TOPIC), logger_name=NODE_NAME)
		self.handle_force_measurement = msg.data

	#^ #######################################
	def _callback_handle_force_estimate(self, msg):
		"""Store handle force estimate and process data."""
		rospy.loginfo_once('Message received on topic {}'.format(FORCE_ESTIMATE_TOPIC), logger_name=NODE_NAME)
		self.handle_force_estimate = msg.data
		self._process()

	#^ #######################################
	def _process(self):
		"""Compute disturbance force estimate and publish on output topic."""
		disturbance_force_estimate = self.handle_force_estimate - self.handle_force_measurement  #@ compute output data
		try:
			self.pub.publish(disturbance_force_estimate)  #@ publish result on output topic
		except rospy.ROSException as e:
			if e.message == 'publish() to a closed topic':
				rospy.logwarn(e.message, logger_name=NODE_NAME)
			else:
				raise

	#^ #######################################
	def spin(self):
		"""Main loop"""
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		while not rospy.is_shutdown():
			rospy.spin()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = EstimateDisturbanceForceNode()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
