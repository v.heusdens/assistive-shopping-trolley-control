#!/usr/bin/env python

"""Node that filters estimated disturbance force using the designed Q-filter."""

import numpy as np
import rospy
import scipy.signal as scp
from collections import deque

from std_msgs.msg import Float64
from trolley_msgs.srv import EmptyTrigger, EmptyTriggerResponse

from toolbox.packages import load_q_filter
from toolbox.ros import load_config

PKG = 'trolley_controller'
NODE_NAME = 'q_filter_node'
PUBLISHER_TOPIC = '/trolley/controller/disturbance_estimate_filtered'
SUBSCRIBER_TOPIC = '/trolley/controller/disturbance_estimate_unfiltered'
SERVICE_TOPIC = '/trolley/tests/reset_q_filter'


#^ FUNCTIONS #######################################
def generate_q_filter_discrete(num_cont, denom_cont, dt):
	"""Returns difference equation of 1st order continuous-time transfer function.
	Uses 0-order hold to transform to discrete time. 
	Output is in the form of y[n] = c u[n-1] - d y[n-1].
	
	Args:
		num_cont (list): numerator of continuous-frequency fransfer function
		denom_cont (list): denominator of continuous-frequency fransfer function
		dt (int): time-step [s]

	Returns:
		y_cur: q-filter difference equation

	Process:
	- Convert from continuous frequency to discrete frequency (zero-order hold):

	   y            a             c             c z^-1
	  ---	   =    -----    =    -----    =    ----------
	   u          s + b         z + d         1 + d z^-1

	- Convert from discrete frequency to discrete time (X*z^-1 --> x[n-1]): 

	  -> y    + d y z^-1 = c u z^-1
	  -> y[n] + d y[n-1] = c u[n-1]  
	  -> y[n] = c u[n-1] - d y[n-1]
	."""
	num_discr, denom_discr, _ = scp.cont2discrete((num_cont, denom_cont), dt)  #@ transform to discrete-time
	num_discr = num_discr.flatten()[-1]
	denom_discr = denom_discr.flatten()[-1]
	rospy.loginfo('Q-filter continuous: {} / {}'.format(num_cont, denom_cont))
	rospy.loginfo('Q-filter discrete:')
	rospy.loginfo('y_cur = {} * u_prev + {} * y_prev'.format(num_discr, -denom_discr), logger_name=NODE_NAME)

	y_cur = lambda u_prev, y_prev: num_discr * u_prev - denom_discr * y_prev  #@ create Q-filter equation
	return y_cur


#^ CLASS #######################################
class QFilterNode:

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_controller = load_config('trolley_controller')
		self.rate = config_controller['update_rate']

		#@ initiate Q-filter function
		q_num, q_denom = load_q_filter()  #@ get Q-filter fraction from config file
		self.q_filter = generate_q_filter_discrete(
		    q_num, q_denom, 1. / self.rate
		)  #@ transform to discrete-time function in the form of y[n] = c u[n-1] - d y[n-1]

		history_size = 1
		self.input_history = deque(np.zeros(history_size), maxlen=history_size)  #@ used for storing previous input
		self.output_history = deque(np.zeros(history_size), maxlen=history_size)  #@ used for storing previous output

		self.active = True  #@ trigger used by reset service

		#^ init publishers/subscribers
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Float64, queue_size=1)
		rospy.Service(SERVICE_TOPIC, EmptyTrigger, self._srv_handle)
		rospy.Subscriber(SUBSCRIBER_TOPIC, Float64, self._callback)

		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def _srv_handle(self, _):
		"""Service call handler"""
		self.active = False
		self.input_history.extend(np.zeros(self.input_history.maxlen))
		self.output_history.extend(np.zeros(self.output_history.maxlen))
		rospy.loginfo('Q-filter history has been reset', logger_name=NODE_NAME)
		self.active = True
		return EmptyTriggerResponse()

	#^ #######################################
	def _callback(self, msg):
		"""Store incoming disturbance estimate data."""
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

		if self.active:
			self.input_history.append(msg.data)

	#^ #######################################
	def _process(self):
		"""Compute filtered disturbance data, publish result on output topic and store for next iteration."""
		dist_filtered = self.q_filter(self.input_history[-1], self.output_history[-1])  #@ compute output data
		self.pub.publish(dist_filtered)  #@ publish result on output topic
		self.output_history.append(dist_filtered)  #@ store result

	#^ #######################################
	def spin(self):
		"""Main loop"""
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)

		r = rospy.Rate(self.rate)  #@ set loop frequency
		while not rospy.is_shutdown():
			self._process()  #@ call main process
			r.sleep()  #@ hold until next iteration should begin


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = QFilterNode()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
