#!/usr/bin/env python

"""Node that actuates the wheel motors by reading from /trolley/actuators/motor_torque topic and publishes required motor command on the '\trolley\wheel_joint_effort_controller/command' topic."""

import rospy

from std_msgs.msg import Float64, Float64MultiArray

from toolbox.ros import load_config

PKG = 'trolley_controller'
NODE_NAME = 'motor_command_writer_node'
PUBLISHER_TOPIC = '/trolley/plugins/wheel_joint_effort_controller/command'
SUBSCRIBER_TOPIC = '/trolley/actuators/motor_torque'


#^ CLASS #######################################
class MotorCommandWriterNode():

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		self.effort = Float64MultiArray()

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Float64MultiArray, queue_size=1)
		self.sub = rospy.Subscriber(SUBSCRIBER_TOPIC, Float64, self._callback)

		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

		#^ shutdown hook
		rospy.on_shutdown(self._shutdown)

	#^ #######################################
	def _callback(self, msg):
		"""Convert motor torque command and publish data on motor torque command topic."""
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

		torque = msg.data
		self.effort.data = [torque, torque]
		self.pub.publish(self.effort)

	#^ #######################################
	def _shutdown(self):
		"""Called upon node shutdown - sets final motor command to 0."""
		rospy.loginfo('Shutdown; setting motor torque to 0', logger_name=NODE_NAME)
		self.sub.unregister()
		self.effort.data = [0, 0]
		self.pub.publish(self.effort)

	#^ #######################################
	def spin(self):
		"""Main loop"""
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		while not rospy.is_shutdown():
			rospy.spin()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = MotorCommandWriterNode()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
