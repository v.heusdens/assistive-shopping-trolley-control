#!/usr/bin/env python

"""Node that reads motor torque, velocity and acceleration topics, then calculates and publishes estimated handle force Fhx."""

import rospy
from sympy import lambdify

from std_msgs.msg import Float64
from trolley_msgs.srv import GetVelocityOffset

from toolbox.params import ParamsNumeric
from toolbox.ros import hold_ros, load_config, wait_on_simulation_start
from toolbox.transform import convert_string_to_sympy

PKG = 'trolley_controller'
NODE_NAME = 'inverse_nominal_model_node'
VELOCITY_TOPIC = '/trolley/sensors/velocity'
ACCELERATION_TOPIC = '/trolley/sensors/acceleration'
FORCE_ESTIMATE_TOPIC = '/trolley/controller/handle_force_estimate'
MOTOR_TORQUE_TOPIC = '/trolley/actuators/motor_torque'
SERVICE_TOPIC = '/trolley/analysis/get_controller_velocity_offset'


#^ CLASS #######################################
class InverseNominalModelNode:

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		#@ load info from config files
		config_general = load_config('trolley_general')
		config_controller = load_config('trolley_controller')
		self.loop_delay = config_general['loop_delay']
		self.velocity_deadband = config_general['velocity_deadband']  #@ configure velocity deadband

		self.active = False  #@ trigger computation and publishing of data

		#@ load inverse nominal model equation
		self.n = ParamsNumeric()  #@ load parameter values
		self.force_model = self._load_model(
		    config_controller['model_Fhx']
		)  #@ load inverse nominal model equation from config file
		self._inverse_nominal_model_eq = lambdify(
		    [self.n.d_xw, self.n.dd_xw, self.n.T_m], self.force_model
		)  #@ transform equation to usable function

		#@ init instances for saving incoming data
		self.velocity_deadband = config_general['velocity_deadband']  #@ configure velocity deadband
		self.velocity_offset = None  #@ used for storing velocity offset
		self.velocity = None  #@ used for storing most recent velocity input
		self.acceleration = None  #@ used for storing most recent acceleration input
		self.motor_torque = 0  #@ used for storing most recent motor torque input

		#@ set if motor torque signal should be used in estimation
		self.feedback = True
		if rospy.has_param('~feedback'):
			self.feedback = rospy.get_param('~feedback')
			rospy.loginfo('feedback trigger set to {} from param server'.format(self.feedback), logger_name=NODE_NAME)
		else:
			rospy.loginfo('feedback trigger set to {}'.format(self.feedback), logger_name=NODE_NAME)

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(FORCE_ESTIMATE_TOPIC, Float64, queue_size=1)
		rospy.Subscriber(VELOCITY_TOPIC, Float64, self._callback_velocity)
		rospy.Subscriber(ACCELERATION_TOPIC, Float64, self._callback_acceleration)
		rospy.Subscriber(MOTOR_TORQUE_TOPIC, Float64, self._callback_motor_torque)
		rospy.Service(SERVICE_TOPIC, GetVelocityOffset, self.srv_handle)

		#@ log messages
		rospy.loginfo('Publisher initiated for {}'.format(FORCE_ESTIMATE_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(VELOCITY_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(ACCELERATION_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(MOTOR_TORQUE_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Service initiated for {}'.format(SERVICE_TOPIC), logger_name=NODE_NAME)

		#^ other
		self._wait_on_simulation_start()
		self._calibration_velocity()

	#^ #######################################
	def _wait_on_simulation_start(self):
		"""Function which terminates as soon as simulation time starts running"""
		wait_on_simulation_start(self.loop_delay, NODE_NAME=NODE_NAME)

	#^ #######################################
	def _load_model(self, model):
		"""Load inverse nominal model equation using nominal parameter values"""
		self.n.set_params_nom()  #@ set params to their nominal values
		self.n.set_params_symbolic(['dd_xw', 'd_xw', 'T_m', 'F_hx', 'F_hy'])  #@ set symbolic params
		self.n.d_beta = 0  #@ set velocity of beta to 0

		return convert_string_to_sympy(model, self.n)  #@ convert config equation to usable function

	#^ #######################################
	def srv_handle(self, _):
		"""Service call handler, returns velocity offset used in controller when controller is calibrated. Otherwise, wait untill timeout is reached and return error."""
		rospy.loginfo('Controller velocity offset requested', logger_name=NODE_NAME)

		timeout = rospy.get_time() + self.srv_timeout

		while self.velocity_offset is None:  #@ wait for offset value to be set
			if rospy.is_shutdown() or rospy.get_time() >= timeout:  #@ timeout condition
				string = "timeout of {} s exceeded before velocity offset could be returned".format(self.srv_timeout)
				rospy.logwarn(string, logger_name=NODE_NAME)
				raise rospy.ServiceException(string)

			rospy.sleep(1. / self.rate)

		rospy.loginfo('Returning controller velocity offset: {}'.format(self.velocity_offset), logger_name=NODE_NAME)
		return self.velocity_offset

	#^ #######################################
	def _calibration_velocity(self):
		"""Determines obtained velocity from gazebo during standstill"""
		hold_ros(0.2, self.loop_delay, NODE_NAME)  #@ wait 0.2 s in simulation time

		self.velocity_offset = rospy.wait_for_message(VELOCITY_TOPIC, Float64).data  #@ receive data
		rospy.loginfo('Controller velocity offset set to {}'.format(self.velocity_offset), logger_name=NODE_NAME)

	#^ CALLBACKS #######################################
	def _callback_motor_torque(self, msg):
		"""Store average motor torque."""
		if not self.feedback:  #@ check if feedback must be used
			rospy.loginfo_once('Feedback turned off, not saving motor torque', logger_name=NODE_NAME)
			return

		rospy.loginfo_once('Message received on topic {}'.format(MOTOR_TORQUE_TOPIC), logger_name=NODE_NAME)
		self.motor_torque = msg.data

	#^ #######################################
	def _callback_velocity(self, msg):
		"""Store velocity measurement after deadbanding the velocity value."""
		rospy.loginfo_once('Message received on topic {}'.format(VELOCITY_TOPIC), logger_name=NODE_NAME)

		if self.velocity_offset is None:  #@ do nothing before velocity offset is set
			return
		velocity = msg.data - self.velocity_offset  #@ apply offset

		#@ apply velocity deadband
		if abs(velocity) < self.velocity_deadband:
			velocity = 0
		self.velocity = velocity

	#^ #######################################
	def _callback_acceleration(self, msg):
		"""Store acceleration measurement and process data."""
		rospy.loginfo_once('Message received on topic {}'.format(ACCELERATION_TOPIC), logger_name=NODE_NAME)

		self.acceleration = msg.data
		if self.active:  #@ perform computations if node is active
			self._process()  #@ call main process

	#^ #######################################
	def _process(self):
		"""Compute handle force estimate and publish on output topic."""
		handle_force_est = self._inverse_nominal_model_eq(
		    self.velocity, self.acceleration, self.motor_torque
		)  #@ compute output data
		try:
			self.pub.publish(handle_force_est)  #@ publish result on output topic
		except rospy.ROSException as e:
			if e.message == 'publish() to a closed topic':
				rospy.logwarn(e.message, logger_name=NODE_NAME)
			else:
				raise

	#^ #######################################
	def spin(self):
		"""Main loop"""
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)

		while not rospy.is_shutdown():
			if self.acceleration is not None and self.velocity is not None:  #@ wait until all sensor data is received
				rospy.loginfo_once("Start publishing on topic {}".format(FORCE_ESTIMATE_TOPIC), logger_name=NODE_NAME)
				self.active = True  #@ activate data processing
			else:
				rospy.loginfo_throttle(
				    0.5, "Waiting until all sensor inputs are received: velocity={}, acceleration={}".format(
				        self.velocity, self.acceleration
				    )
				)

			rospy.spin()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = InverseNominalModelNode()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
